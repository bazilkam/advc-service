package ua.nano.soap.impl;

import advcash.wsm.*;
import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import ua.nano.rest.dto.OnlineCurrentTimeDTO;
import ua.nano.rest.dto.TransactionHistoryRequestDTO;
import ua.nano.rest.dto.TransactionHistoryResponseDTO;
import ua.nano.rest.service.OnlineCurrentTime;
import ua.nano.soap.AdvCashClientAPI;

import java.lang.reflect.Field;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class AdvCashClientAPIImpl extends WebServiceGatewaySupport implements AdvCashClientAPI {

    @Autowired
    OnlineCurrentTime currentTime;

    private static final Logger log = LoggerFactory.getLogger(AdvCashClientAPIImpl.class);

    private MerchantWebService_Service service = new MerchantWebService_Service();
    private MerchantWebService ws = service.getMerchantWebServicePort();

    public String sendMoneyToAccountRequest(SendMoney request) throws Exception {
        log.debug("send money request: to: {} amount: {}", request.getArg1().getEmail(), request.getArg1().getAmount());
        request.getArg0().setAuthenticationToken(getToken(request.getArg0()));
        return this.ws.sendMoney(request.getArg0(), request.getArg1());
    }

    public List<TransactionHistoryResponseDTO> getTransactionsHistory(TransactionHistoryRequestDTO request) throws Exception {
        System.out.println(request.getClass().getDeclaredFields().length);
        log.debug("geting tramsactions history: dateFrom: {}, dateTo: {}", request.getArg1().getStartTimeFrom(), request.getArg1().getStartTimeTo());
        request.getArg0().setAuthenticationToken(getToken(request.getArg0()));
        return parseToHistoryResponseDTO(this.ws.history(request.getArg0(), request.getArg1()));
    }

    private String getToken(AuthDTO authDTO) {
        OnlineCurrentTimeDTO moscowTime = currentTime.getMoscowCurrentTime().getBody();
        LocalDateTime ldt = LocalDateTime.parse(moscowTime.getTime(), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"))
                .minusMinutes(moscowTime.getOffset());
        StringBuilder builder = new StringBuilder();
        builder.append(authDTO.getAuthenticationToken())
                .append(":")
                .append(DateTimeFormatter.ofPattern("yyyyMMdd:HH").format(ldt));
        return DigestUtils.sha256Hex(builder.toString().getBytes());
    }

    private List<TransactionHistoryResponseDTO> parseToHistoryResponseDTO(List<OutcomingTransactionDTO> transactions) {
        return transactions.stream().map(TransactionHistoryResponseDTO::new).collect(Collectors.toList());
    }

    public void printFieldNames(TransactionHistoryRequestDTO obj) {
        for (Field field : obj.getClass().getFields()) {
            System.out.println(field.getName());
        }
    }
}
