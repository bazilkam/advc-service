package ua.nano.soap;

import advcash.wsm.SendMoney;
import ua.nano.rest.dto.TransactionHistoryRequestDTO;
import ua.nano.rest.dto.TransactionHistoryResponseDTO;

import java.util.List;

public interface AdvCashClientAPI {
    String sendMoneyToAccountRequest(SendMoney request) throws Exception;

    List<TransactionHistoryResponseDTO> getTransactionsHistory(TransactionHistoryRequestDTO request) throws Exception;
}
