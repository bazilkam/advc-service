package ua.nano.rest.dto;

import advcash.wsm.OutcomingTransactionDTO;

public class TransactionHistoryResponseDTO {
    private String type;
    private String currency;
    private String amount;
    private String fee;
    private String payee;
    private String payer;
    private String comment;

    public TransactionHistoryResponseDTO() {
    }

    public TransactionHistoryResponseDTO(String type, String currency, String amount, String fee, String payee, String payer, String memo) {
        this.type = type;
        this.currency = currency;
        this.amount = amount;
        this.fee = fee;
        this.payee = payee;
        this.payer = payer;
        this.comment = memo;
    }

    public TransactionHistoryResponseDTO(OutcomingTransactionDTO item) {
        this.type = item.getDirection().name();
        this.currency = item.getCurrency().name();
        this.amount = item.getAmount().toString();
        this.fee = item.getFullCommission().toString();
        this.payee = item.getReceiverEmail();
        this.payer = item.getSenderEmail();
        this.comment = item.getComment();
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getFee() {
        return fee;
    }

    public void setFee(String fee) {
        this.fee = fee;
    }

    public String getPayee() {
        return payee;
    }

    public void setPayee(String payee) {
        this.payee = payee;
    }

    public String getPayer() {
        return payer;
    }

    public void setPayer(String payer) {
        this.payer = payer;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
