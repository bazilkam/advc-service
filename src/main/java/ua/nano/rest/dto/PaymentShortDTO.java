package ua.nano.rest.dto;

public class PaymentShortDTO {
    private String ac_account_email;
    private String ac_sci_name;
    private Double ac_amount;
    private String ac_currency;
    private String ac_order_id;
    private String ac_sign;
    private String ac_comments;
    private String operation_id;
    private String login;

    public String getAc_account_email() {
        return ac_account_email;
    }

    public void setAc_account_email(String ac_account_email) {
        this.ac_account_email = ac_account_email;
    }

    public String getAc_sci_name() {
        return ac_sci_name;
    }

    public void setAc_sci_name(String ac_sci_name) {
        this.ac_sci_name = ac_sci_name;
    }

    public Double getAc_amount() {
        return ac_amount;
    }

    public void setAc_amount(Double ac_amount) {
        this.ac_amount = ac_amount;
    }

    public String getAc_currency() {
        return ac_currency;
    }

    public void setAc_currency(String ac_currency) {
        this.ac_currency = ac_currency;
    }

    public String getAc_order_id() {
        return ac_order_id;
    }

    public void setAc_order_id(String ac_order_id) {
        this.ac_order_id = ac_order_id;
    }

    public String getAc_sign() {
        return ac_sign;
    }

    public void setAc_sign(String ac_sign) {
        this.ac_sign = ac_sign;
    }

    public String getAc_comments() {
        return ac_comments;
    }

    public void setAc_comments(String ac_comments) {
        this.ac_comments = ac_comments;
    }

    public String getOperation_id() {
        return operation_id;
    }

    public void setOperation_id(String operation_id) {
        this.operation_id = operation_id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }
}
