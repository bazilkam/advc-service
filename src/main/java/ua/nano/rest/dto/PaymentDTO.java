package ua.nano.rest.dto;

import advcash.wsm.Currency;

public class PaymentDTO {
    private String accountEmail;
    private String sciName;
    private Double amount;
    private Currency currency;
    private String orderId;
    private String sign;
    private String buyerWallet;
    private String comment;
    private String successUrl;
    private String successUrlMethod;
    private String failUrl;
    private String statusUrl;
    private String statusUrlMethod;
    private String customField;

    public String getAccountEmail() {
        return accountEmail;
    }

    public void setAccountEmail(String accountEmail) {
        this.accountEmail = accountEmail;
    }

    public String getSciName() {
        return sciName;
    }

    public void setSciName(String sciName) {
        this.sciName = sciName;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public String getBuyerWallet() {
        return buyerWallet;
    }

    public void setBuyerWallet(String buyerWallet) {
        this.buyerWallet = buyerWallet;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getSuccessUrl() {
        return successUrl;
    }

    public void setSuccessUrl(String successUrl) {
        this.successUrl = successUrl;
    }

    public String getSuccessUrlMethod() {
        return successUrlMethod;
    }

    public void setSuccessUrlMethod(String successUrlMethod) {
        this.successUrlMethod = successUrlMethod;
    }

    public String getFailUrl() {
        return failUrl;
    }

    public void setFailUrl(String failUrl) {
        this.failUrl = failUrl;
    }

    public String getStatusUrl() {
        return statusUrl;
    }

    public void setStatusUrl(String statusUrl) {
        this.statusUrl = statusUrl;
    }

    public String getStatusUrlMethod() {
        return statusUrlMethod;
    }

    public void setStatusUrlMethod(String statusUrlMethod) {
        this.statusUrlMethod = statusUrlMethod;
    }

    public String getCustomField() {
        return customField;
    }

    public void setCustomField(String customField) {
        this.customField = customField;
    }
}
