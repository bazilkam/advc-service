package ua.nano.rest.dto;

import advcash.wsm.AuthDTO;
import advcash.wsm.MerchantAPITransactionFilter;

public class TransactionHistoryRequestDTO {
    private AuthDTO arg0;
    private MerchantAPITransactionFilter arg1;

    public AuthDTO getArg0() {
        return arg0;
    }

    public void setArg0(AuthDTO arg0) {
        this.arg0 = arg0;
    }

    public MerchantAPITransactionFilter getArg1() {
        return arg1;
    }

    public void setArg1(MerchantAPITransactionFilter arg1) {
        this.arg1 = arg1;
    }
}
