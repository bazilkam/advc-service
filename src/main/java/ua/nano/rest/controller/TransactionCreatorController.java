package ua.nano.rest.controller;

import advcash.wsm.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ua.nano.rest.dto.PaymentDTO;
import ua.nano.rest.dto.PaymentShortDTO;
import ua.nano.rest.dto.TransactionHistoryRequestDTO;
import ua.nano.rest.dto.TransactionHistoryResponseDTO;
import ua.nano.rest.service.FormService;
import ua.nano.soap.AdvCashClientAPI;

import java.util.List;

@RestController
@RequestMapping("/api/")
public class TransactionCreatorController {

    @Autowired
    AdvCashClientAPI client;

    @Autowired
    FormService formService;

    @RequestMapping(path = "sendMoney", method = RequestMethod.POST)
    public String sendMoneyRequest(@RequestBody SendMoney request) throws Exception {
        return client.sendMoneyToAccountRequest(request);
    }

    @RequestMapping(path = "getTransactionHistory", method = RequestMethod.POST)
    public List<TransactionHistoryResponseDTO> getTransactionHistoryRequest(@RequestBody TransactionHistoryRequestDTO request) throws Exception {
        return client.getTransactionsHistory(request);
    }

    @RequestMapping(path = "getPaymentForm", method = RequestMethod.POST)
    public String getPaymentForm(@RequestBody PaymentShortDTO request) throws Exception {
        return formService.getCurrentShortForm("payment", request);
    }

    @RequestMapping(path = "hello", method = RequestMethod.GET)
    public String getHello() throws Exception {
        return "Hello world!";
    }

}
