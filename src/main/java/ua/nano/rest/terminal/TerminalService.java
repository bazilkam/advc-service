package ua.nano.rest.terminal;

import org.springframework.stereotype.Service;

@Service
public class TerminalService {

    public Status verifyRequest(TerminalRrequestDTO request){
        if ("1234".equals(request.getPincode())){
            return Status.APPROVE;
        }else {
            return Status.CANCELED;
        }
    }

}
