package ua.nano.rest.terminal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/terminal/")
public class TerminalController {

    @Autowired
    private TerminalService service;

    @RequestMapping(path = "test", method = RequestMethod.GET)
    public String test() {
        return "TEST SUCCESSFUL";
    }

    @RequestMapping(path = "sendMoney", method = RequestMethod.POST)
    public String sendMoneyRequest(@RequestBody TerminalRrequestDTO request) {
        return service.verifyRequest(request).name();
    }
}
