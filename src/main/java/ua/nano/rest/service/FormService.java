package ua.nano.rest.service;

import ua.nano.rest.dto.PaymentDTO;
import ua.nano.rest.dto.PaymentShortDTO;

import java.io.FileNotFoundException;

public interface FormService {
    String getCurrentForm(String formName, PaymentDTO request) throws Exception;
    String getCurrentShortForm(String formName, PaymentShortDTO request) throws Exception;
}
