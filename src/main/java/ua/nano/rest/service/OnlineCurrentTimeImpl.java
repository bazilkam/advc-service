package ua.nano.rest.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import ua.nano.rest.dto.OnlineCurrentTimeDTO;
import ua.nano.Constants;

import java.util.Collections;

@Service
public class OnlineCurrentTimeImpl implements OnlineCurrentTime {

    @Value("${url.currenttimeapi}")
    private String url;

    @Override
    public ResponseEntity<OnlineCurrentTimeDTO> getMoscowCurrentTime() {
        RestTemplate template = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<>("parameters", headers);
        return template.exchange(url + Constants.LOCATION_MOSCOW, HttpMethod.GET, entity, OnlineCurrentTimeDTO.class);
    }
}
