package ua.nano.rest.service;

import org.springframework.http.ResponseEntity;
import ua.nano.rest.dto.OnlineCurrentTimeDTO;

public interface OnlineCurrentTime {
    ResponseEntity<OnlineCurrentTimeDTO> getMoscowCurrentTime();
}
