package ua.nano.rest.service;


import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.FormElement;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import ua.nano.rest.dto.PaymentDTO;
import ua.nano.rest.dto.PaymentShortDTO;

import java.io.InputStream;
import java.util.Scanner;

@Service
public class FormServiceImpl implements FormService {
    @Autowired
    private ApplicationContext context;

    public String getCurrentForm(String formName, PaymentDTO request) throws Exception {
        String form = parseHtml(formName).toString();
        Document doc = Jsoup.parse(form);
        FormElement formElement = (FormElement) doc.select("form").first();
        Elements elements = formElement.getElementsByAttribute("name");

        elements.select("[name=ac_account_email]").attr("value", request.getAccountEmail());
        elements.select("[name=ac_sci_name]").attr("value", request.getSciName());
        elements.select("[name=ac_amount]").attr("value", request.getAmount().toString());
        elements.select("[name=ac_currency]").attr("value", request.getCurrency().name());
        elements.select("[name=ac_order_id]").attr("value", request.getOrderId());
        elements.select("[name=ac_sign]").attr("value", request.getSign());
        elements.select("[name=ac_comments]").attr("value", request.getComment());
        elements.select("[name=operation_id]").attr("value", request.getOrderId());
        elements.select("[name=login]").attr("value", request.getBuyerWallet());

        System.out.println(doc.body().toString());
        return doc.body().toString();
    }

    @Override
    public String getCurrentShortForm(String formName, PaymentShortDTO request) throws Exception {
        String form = parseHtml(formName);
        Document doc = Jsoup.parse(form);
        FormElement formElement = (FormElement) doc.select("form").first();
        Elements elements = formElement.getElementsByAttribute("name");

        elements.select("[name=ac_account_email]").attr("value", request.getAc_account_email());
        elements.select("[name=ac_sci_name]").attr("value", request.getAc_sci_name());
        elements.select("[name=ac_amount]").attr("value", request.getAc_amount().toString());
        elements.select("[name=ac_currency]").attr("value", request.getAc_currency());
        elements.select("[name=ac_order_id]").attr("value", request.getAc_order_id());
        elements.select("[name=ac_sign]").attr("value", request.getAc_sign());
        elements.select("[name=ac_comments]").attr("value", request.getAc_comments());
        elements.select("[name=operation_id]").attr("value", request.getOperation_id());
        elements.select("[name=login]").attr("value", request.getLogin());

        System.out.println(doc.body().toString());
        return doc.body().toString();
    }

    public String parseHtml(String formName) throws Exception {
        InputStream inputStream = context.getResource("classpath:" + formName + ".html").getInputStream();
        if (inputStream == null) {
            throw new Exception("input stream in null");
        }
        Scanner scanner = new Scanner(inputStream).useDelimiter("\\A");
        return scanner.hasNext() ? scanner.next() : "";
    }

}
