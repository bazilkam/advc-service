
package advcash.wsm;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for createBitcoinInvoiceResult complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="createBitcoinInvoiceResult">
 *   &lt;complexContent>
 *     &lt;extension base="{http://wsm.advcash/}createBitcoinInvoiceRequest">
 *       &lt;sequence>
 *         &lt;element name="bitcoinAddress" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="bitcoinAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "createBitcoinInvoiceResult", propOrder = {
    "bitcoinAddress",
    "bitcoinAmount"
})
public class CreateBitcoinInvoiceResult
    extends CreateBitcoinInvoiceRequest
{

    protected String bitcoinAddress;
    protected BigDecimal bitcoinAmount;

    /**
     * Gets the value of the bitcoinAddress property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBitcoinAddress() {
        return bitcoinAddress;
    }

    /**
     * Sets the value of the bitcoinAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBitcoinAddress(String value) {
        this.bitcoinAddress = value;
    }

    /**
     * Gets the value of the bitcoinAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getBitcoinAmount() {
        return bitcoinAmount;
    }

    /**
     * Sets the value of the bitcoinAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setBitcoinAmount(BigDecimal value) {
        this.bitcoinAmount = value;
    }

}
