
package advcash.wsm;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for checkCurrencyExchangeResultHolder complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="checkCurrencyExchangeResultHolder">
 *   &lt;complexContent>
 *     &lt;extension base="{http://wsm.advcash/}checkCurrencyExchangeRequest">
 *       &lt;sequence>
 *         &lt;element name="amountExchanged" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="rate" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "checkCurrencyExchangeResultHolder", propOrder = {
    "amountExchanged",
    "rate"
})
public class CheckCurrencyExchangeResultHolder
    extends CheckCurrencyExchangeRequest
{

    protected BigDecimal amountExchanged;
    protected BigDecimal rate;

    /**
     * Gets the value of the amountExchanged property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAmountExchanged() {
        return amountExchanged;
    }

    /**
     * Sets the value of the amountExchanged property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAmountExchanged(BigDecimal value) {
        this.amountExchanged = value;
    }

    /**
     * Gets the value of the rate property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRate() {
        return rate;
    }

    /**
     * Sets the value of the rate property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRate(BigDecimal value) {
        this.rate = value;
    }

}
