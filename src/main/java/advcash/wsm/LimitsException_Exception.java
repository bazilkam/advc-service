
package advcash.wsm;

import javax.xml.ws.WebFault;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.9-b130926.1035
 * Generated source version: 2.2
 * 
 */
@WebFault(name = "LimitsException", targetNamespace = "http://wsm.advcash/")
public class LimitsException_Exception
    extends Exception
{

    /**
     * Java type that goes as soapenv:Fault detail element.
     * 
     */
    private LimitsException faultInfo;

    /**
     * 
     * @param faultInfo
     * @param message
     */
    public LimitsException_Exception(String message, LimitsException faultInfo) {
        super(message);
        this.faultInfo = faultInfo;
    }

    /**
     * 
     * @param faultInfo
     * @param cause
     * @param message
     */
    public LimitsException_Exception(String message, LimitsException faultInfo, Throwable cause) {
        super(message, cause);
        this.faultInfo = faultInfo;
    }

    /**
     * 
     * @return
     *     returns fault bean: advcash.wsm.LimitsException
     */
    public LimitsException getFaultInfo() {
        return faultInfo;
    }

}
