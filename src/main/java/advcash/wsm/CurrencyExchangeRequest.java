
package advcash.wsm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for currencyExchangeRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="currencyExchangeRequest">
 *   &lt;complexContent>
 *     &lt;extension base="{http://wsm.advcash/}moneyRequest">
 *       &lt;sequence>
 *         &lt;element name="action" type="{http://wsm.advcash/}currencyExchangeAction" minOccurs="0"/>
 *         &lt;element name="from" type="{http://wsm.advcash/}currency" minOccurs="0"/>
 *         &lt;element name="to" type="{http://wsm.advcash/}currency" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "currencyExchangeRequest", propOrder = {
    "action",
    "from",
    "to"
})
public class CurrencyExchangeRequest
    extends MoneyRequest
{

    @XmlSchemaType(name = "string")
    protected CurrencyExchangeAction action;
    @XmlSchemaType(name = "string")
    protected Currency from;
    @XmlSchemaType(name = "string")
    protected Currency to;

    /**
     * Gets the value of the action property.
     * 
     * @return
     *     possible object is
     *     {@link CurrencyExchangeAction }
     *     
     */
    public CurrencyExchangeAction getAction() {
        return action;
    }

    /**
     * Sets the value of the action property.
     * 
     * @param value
     *     allowed object is
     *     {@link CurrencyExchangeAction }
     *     
     */
    public void setAction(CurrencyExchangeAction value) {
        this.action = value;
    }

    /**
     * Gets the value of the from property.
     * 
     * @return
     *     possible object is
     *     {@link Currency }
     *     
     */
    public Currency getFrom() {
        return from;
    }

    /**
     * Sets the value of the from property.
     * 
     * @param value
     *     allowed object is
     *     {@link Currency }
     *     
     */
    public void setFrom(Currency value) {
        this.from = value;
    }

    /**
     * Gets the value of the to property.
     * 
     * @return
     *     possible object is
     *     {@link Currency }
     *     
     */
    public Currency getTo() {
        return to;
    }

    /**
     * Sets the value of the to property.
     * 
     * @param value
     *     allowed object is
     *     {@link Currency }
     *     
     */
    public void setTo(Currency value) {
        this.to = value;
    }

}
