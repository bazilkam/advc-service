
package advcash.wsm;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for currencyExchangeAction.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="currencyExchangeAction">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="BUY"/>
 *     &lt;enumeration value="SELL"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "currencyExchangeAction")
@XmlEnum
public enum CurrencyExchangeAction {

    BUY,
    SELL;

    public String value() {
        return name();
    }

    public static CurrencyExchangeAction fromValue(String v) {
        return valueOf(v);
    }

}
