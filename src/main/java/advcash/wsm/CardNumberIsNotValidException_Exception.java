
package advcash.wsm;

import javax.xml.ws.WebFault;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.9-b130926.1035
 * Generated source version: 2.2
 * 
 */
@WebFault(name = "CardNumberIsNotValidException", targetNamespace = "http://wsm.advcash/")
public class CardNumberIsNotValidException_Exception
    extends Exception
{

    /**
     * Java type that goes as soapenv:Fault detail element.
     * 
     */
    private CardNumberIsNotValidException faultInfo;

    /**
     * 
     * @param faultInfo
     * @param message
     */
    public CardNumberIsNotValidException_Exception(String message, CardNumberIsNotValidException faultInfo) {
        super(message);
        this.faultInfo = faultInfo;
    }

    /**
     * 
     * @param faultInfo
     * @param cause
     * @param message
     */
    public CardNumberIsNotValidException_Exception(String message, CardNumberIsNotValidException faultInfo, Throwable cause) {
        super(message, cause);
        this.faultInfo = faultInfo;
    }

    /**
     * 
     * @return
     *     returns fault bean: advcash.wsm.CardNumberIsNotValidException
     */
    public CardNumberIsNotValidException getFaultInfo() {
        return faultInfo;
    }

}
