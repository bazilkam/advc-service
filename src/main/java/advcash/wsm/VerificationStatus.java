
package advcash.wsm;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for verificationStatus.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="verificationStatus">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Verified"/>
 *     &lt;enumeration value="NotVerified"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "verificationStatus")
@XmlEnum
public enum VerificationStatus {

    @XmlEnumValue("Verified")
    VERIFIED("Verified"),
    @XmlEnumValue("NotVerified")
    NOT_VERIFIED("NotVerified");
    private final String value;

    VerificationStatus(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static VerificationStatus fromValue(String v) {
        for (VerificationStatus c: VerificationStatus.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
