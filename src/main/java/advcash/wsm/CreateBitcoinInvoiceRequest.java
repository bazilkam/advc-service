
package advcash.wsm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for createBitcoinInvoiceRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="createBitcoinInvoiceRequest">
 *   &lt;complexContent>
 *     &lt;extension base="{http://wsm.advcash/}moneyRequest">
 *       &lt;sequence>
 *         &lt;element name="orderId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="sciName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "createBitcoinInvoiceRequest", propOrder = {
    "orderId",
    "sciName"
})
@XmlSeeAlso({
    CreateBitcoinInvoiceResult.class
})
public class CreateBitcoinInvoiceRequest
    extends MoneyRequest
{

    protected String orderId;
    protected String sciName;

    /**
     * Gets the value of the orderId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrderId() {
        return orderId;
    }

    /**
     * Sets the value of the orderId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrderId(String value) {
        this.orderId = value;
    }

    /**
     * Gets the value of the sciName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSciName() {
        return sciName;
    }

    /**
     * Sets the value of the sciName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSciName(String value) {
        this.sciName = value;
    }

}
