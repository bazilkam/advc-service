
package advcash.wsm;

import javax.xml.ws.WebFault;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.9-b130926.1035
 * Generated source version: 2.2
 * 
 */
@WebFault(name = "WalletDoesNotExist", targetNamespace = "http://wsm.advcash/")
public class WalletDoesNotExist_Exception
    extends Exception
{

    /**
     * Java type that goes as soapenv:Fault detail element.
     * 
     */
    private WalletDoesNotExist faultInfo;

    /**
     * 
     * @param faultInfo
     * @param message
     */
    public WalletDoesNotExist_Exception(String message, WalletDoesNotExist faultInfo) {
        super(message);
        this.faultInfo = faultInfo;
    }

    /**
     * 
     * @param faultInfo
     * @param cause
     * @param message
     */
    public WalletDoesNotExist_Exception(String message, WalletDoesNotExist faultInfo, Throwable cause) {
        super(message, cause);
        this.faultInfo = faultInfo;
    }

    /**
     * 
     * @return
     *     returns fault bean: advcash.wsm.WalletDoesNotExist
     */
    public WalletDoesNotExist getFaultInfo() {
        return faultInfo;
    }

}
