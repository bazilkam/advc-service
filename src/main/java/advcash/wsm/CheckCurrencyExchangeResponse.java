
package advcash.wsm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for checkCurrencyExchangeResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="checkCurrencyExchangeResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="return" type="{http://wsm.advcash/}checkCurrencyExchangeResultHolder" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "checkCurrencyExchangeResponse", propOrder = {
    "_return"
})
public class CheckCurrencyExchangeResponse {

    @XmlElement(name = "return")
    protected CheckCurrencyExchangeResultHolder _return;

    /**
     * Gets the value of the return property.
     * 
     * @return
     *     possible object is
     *     {@link CheckCurrencyExchangeResultHolder }
     *     
     */
    public CheckCurrencyExchangeResultHolder getReturn() {
        return _return;
    }

    /**
     * Sets the value of the return property.
     * 
     * @param value
     *     allowed object is
     *     {@link CheckCurrencyExchangeResultHolder }
     *     
     */
    public void setReturn(CheckCurrencyExchangeResultHolder value) {
        this._return = value;
    }

}
