
package advcash.wsm;

import javax.xml.ws.WebFault;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.9-b130926.1035
 * Generated source version: 2.2
 * 
 */
@WebFault(name = "NotEnoughMoneyException", targetNamespace = "http://wsm.advcash/")
public class NotEnoughMoneyException_Exception
    extends Exception
{

    /**
     * Java type that goes as soapenv:Fault detail element.
     * 
     */
    private NotEnoughMoneyException faultInfo;

    /**
     * 
     * @param faultInfo
     * @param message
     */
    public NotEnoughMoneyException_Exception(String message, NotEnoughMoneyException faultInfo) {
        super(message);
        this.faultInfo = faultInfo;
    }

    /**
     * 
     * @param faultInfo
     * @param cause
     * @param message
     */
    public NotEnoughMoneyException_Exception(String message, NotEnoughMoneyException faultInfo, Throwable cause) {
        super(message, cause);
        this.faultInfo = faultInfo;
    }

    /**
     * 
     * @return
     *     returns fault bean: advcash.wsm.NotEnoughMoneyException
     */
    public NotEnoughMoneyException getFaultInfo() {
        return faultInfo;
    }

}
