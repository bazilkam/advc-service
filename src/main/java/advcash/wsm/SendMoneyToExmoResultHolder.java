
package advcash.wsm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for sendMoneyToExmoResultHolder complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="sendMoneyToExmoResultHolder">
 *   &lt;complexContent>
 *     &lt;extension base="{http://wsm.advcash/}sendMoneyToMarketResultHolder">
 *       &lt;sequence>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "sendMoneyToExmoResultHolder")
public class SendMoneyToExmoResultHolder
    extends SendMoneyToMarketResultHolder
{


}
