
package advcash.wsm;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for cardType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="cardType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="VIRTUAL"/>
 *     &lt;enumeration value="PLASTIC"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "cardType")
@XmlEnum
public enum CardType {

    VIRTUAL,
    PLASTIC;

    public String value() {
        return name();
    }

    public static CardType fromValue(String v) {
        return valueOf(v);
    }

}
