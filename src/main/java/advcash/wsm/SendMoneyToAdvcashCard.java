
package advcash.wsm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for sendMoneyToAdvcashCard complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="sendMoneyToAdvcashCard">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="arg0" type="{http://wsm.advcash/}authDTO" minOccurs="0"/>
 *         &lt;element name="arg1" type="{http://wsm.advcash/}advcashCardTransferRequest" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "sendMoneyToAdvcashCard", propOrder = {
    "arg0",
    "arg1"
})
public class SendMoneyToAdvcashCard {

    protected AuthDTO arg0;
    protected AdvcashCardTransferRequest arg1;

    /**
     * Gets the value of the arg0 property.
     * 
     * @return
     *     possible object is
     *     {@link AuthDTO }
     *     
     */
    public AuthDTO getArg0() {
        return arg0;
    }

    /**
     * Sets the value of the arg0 property.
     * 
     * @param value
     *     allowed object is
     *     {@link AuthDTO }
     *     
     */
    public void setArg0(AuthDTO value) {
        this.arg0 = value;
    }

    /**
     * Gets the value of the arg1 property.
     * 
     * @return
     *     possible object is
     *     {@link AdvcashCardTransferRequest }
     *     
     */
    public AdvcashCardTransferRequest getArg1() {
        return arg1;
    }

    /**
     * Sets the value of the arg1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link AdvcashCardTransferRequest }
     *     
     */
    public void setArg1(AdvcashCardTransferRequest value) {
        this.arg1 = value;
    }

}
