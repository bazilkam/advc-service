
package advcash.wsm;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for withdrawToEcurrencyRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="withdrawToEcurrencyRequest">
 *   &lt;complexContent>
 *     &lt;extension base="{http://wsm.advcash/}moneyRequest">
 *       &lt;sequence>
 *         &lt;element name="btcAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="ecurrency" type="{http://wsm.advcash/}ecurrency" minOccurs="0"/>
 *         &lt;element name="ethAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="receiver" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "withdrawToEcurrencyRequest", propOrder = {
    "btcAmount",
    "ecurrency",
    "ethAmount",
    "receiver"
})
public class WithdrawToEcurrencyRequest
    extends MoneyRequest
{

    protected BigDecimal btcAmount;
    @XmlSchemaType(name = "string")
    protected Ecurrency ecurrency;
    protected BigDecimal ethAmount;
    protected String receiver;

    /**
     * Gets the value of the btcAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getBtcAmount() {
        return btcAmount;
    }

    /**
     * Sets the value of the btcAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setBtcAmount(BigDecimal value) {
        this.btcAmount = value;
    }

    /**
     * Gets the value of the ecurrency property.
     * 
     * @return
     *     possible object is
     *     {@link Ecurrency }
     *     
     */
    public Ecurrency getEcurrency() {
        return ecurrency;
    }

    /**
     * Sets the value of the ecurrency property.
     * 
     * @param value
     *     allowed object is
     *     {@link Ecurrency }
     *     
     */
    public void setEcurrency(Ecurrency value) {
        this.ecurrency = value;
    }

    /**
     * Gets the value of the ethAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getEthAmount() {
        return ethAmount;
    }

    /**
     * Sets the value of the ethAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setEthAmount(BigDecimal value) {
        this.ethAmount = value;
    }

    /**
     * Gets the value of the receiver property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReceiver() {
        return receiver;
    }

    /**
     * Sets the value of the receiver property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReceiver(String value) {
        this.receiver = value;
    }

}
