
package advcash.wsm;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for supportedLanguage.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="supportedLanguage">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="en"/>
 *     &lt;enumeration value="ru"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "supportedLanguage")
@XmlEnum
public enum SupportedLanguage {

    @XmlEnumValue("en")
    EN("en"),
    @XmlEnumValue("ru")
    RU("ru");
    private final String value;

    SupportedLanguage(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static SupportedLanguage fromValue(String v) {
        for (SupportedLanguage c: SupportedLanguage.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
