
package advcash.wsm;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for withdrawalThroughExternalPaymentSystemRequestDTO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="withdrawalThroughExternalPaymentSystemRequestDTO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="amount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="comment" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="currency" type="{http://wsm.advcash/}currency" minOccurs="0"/>
 *         &lt;element name="externalPaymentSystem" type="{http://wsm.advcash/}externalSystemWithdrawalType" minOccurs="0"/>
 *         &lt;element name="receiver" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="savePaymentTemplate" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "withdrawalThroughExternalPaymentSystemRequestDTO", propOrder = {
    "amount",
    "comment",
    "currency",
    "externalPaymentSystem",
    "receiver",
    "savePaymentTemplate"
})
public class WithdrawalThroughExternalPaymentSystemRequestDTO {

    protected BigDecimal amount;
    protected String comment;
    @XmlSchemaType(name = "string")
    protected Currency currency;
    @XmlSchemaType(name = "string")
    protected ExternalSystemWithdrawalType externalPaymentSystem;
    protected String receiver;
    protected boolean savePaymentTemplate;

    /**
     * Gets the value of the amount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * Sets the value of the amount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAmount(BigDecimal value) {
        this.amount = value;
    }

    /**
     * Gets the value of the comment property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComment() {
        return comment;
    }

    /**
     * Sets the value of the comment property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComment(String value) {
        this.comment = value;
    }

    /**
     * Gets the value of the currency property.
     * 
     * @return
     *     possible object is
     *     {@link Currency }
     *     
     */
    public Currency getCurrency() {
        return currency;
    }

    /**
     * Sets the value of the currency property.
     * 
     * @param value
     *     allowed object is
     *     {@link Currency }
     *     
     */
    public void setCurrency(Currency value) {
        this.currency = value;
    }

    /**
     * Gets the value of the externalPaymentSystem property.
     * 
     * @return
     *     possible object is
     *     {@link ExternalSystemWithdrawalType }
     *     
     */
    public ExternalSystemWithdrawalType getExternalPaymentSystem() {
        return externalPaymentSystem;
    }

    /**
     * Sets the value of the externalPaymentSystem property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExternalSystemWithdrawalType }
     *     
     */
    public void setExternalPaymentSystem(ExternalSystemWithdrawalType value) {
        this.externalPaymentSystem = value;
    }

    /**
     * Gets the value of the receiver property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReceiver() {
        return receiver;
    }

    /**
     * Sets the value of the receiver property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReceiver(String value) {
        this.receiver = value;
    }

    /**
     * Gets the value of the savePaymentTemplate property.
     * 
     */
    public boolean isSavePaymentTemplate() {
        return savePaymentTemplate;
    }

    /**
     * Sets the value of the savePaymentTemplate property.
     * 
     */
    public void setSavePaymentTemplate(boolean value) {
        this.savePaymentTemplate = value;
    }

}
