
package advcash.wsm;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ecurrency.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ecurrency">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="BITCOIN"/>
 *     &lt;enumeration value="CAPITALIST"/>
 *     &lt;enumeration value="ECOIN"/>
 *     &lt;enumeration value="OKPAY"/>
 *     &lt;enumeration value="PAXUM"/>
 *     &lt;enumeration value="PAYEER"/>
 *     &lt;enumeration value="PAYZA"/>
 *     &lt;enumeration value="PERFECT_MONEY"/>
 *     &lt;enumeration value="WEB_MONEY"/>
 *     &lt;enumeration value="QIWI"/>
 *     &lt;enumeration value="YANDEX_MONEY"/>
 *     &lt;enumeration value="EPESE"/>
 *     &lt;enumeration value="ETHEREUM"/>
 *     &lt;enumeration value="BITCOIN_CASH"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ecurrency")
@XmlEnum
public enum Ecurrency {

    BITCOIN,
    CAPITALIST,
    ECOIN,
    OKPAY,
    PAXUM,
    PAYEER,
    PAYZA,
    PERFECT_MONEY,
    WEB_MONEY,
    QIWI,
    YANDEX_MONEY,
    EPESE,
    ETHEREUM,
    BITCOIN_CASH;

    public String value() {
        return name();
    }

    public static Ecurrency fromValue(String v) {
        return valueOf(v);
    }

}
