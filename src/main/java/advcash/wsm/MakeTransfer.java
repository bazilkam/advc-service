
package advcash.wsm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for makeTransfer complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="makeTransfer">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="arg0" type="{http://wsm.advcash/}authDTO" minOccurs="0"/>
 *         &lt;element name="arg1" type="{http://wsm.advcash/}typeOfTransaction" minOccurs="0"/>
 *         &lt;element name="arg2" type="{http://wsm.advcash/}transferRequestDTO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "makeTransfer", propOrder = {
    "arg0",
    "arg1",
    "arg2"
})
public class MakeTransfer {

    protected AuthDTO arg0;
    @XmlSchemaType(name = "string")
    protected TypeOfTransaction arg1;
    protected TransferRequestDTO arg2;

    /**
     * Gets the value of the arg0 property.
     * 
     * @return
     *     possible object is
     *     {@link AuthDTO }
     *     
     */
    public AuthDTO getArg0() {
        return arg0;
    }

    /**
     * Sets the value of the arg0 property.
     * 
     * @param value
     *     allowed object is
     *     {@link AuthDTO }
     *     
     */
    public void setArg0(AuthDTO value) {
        this.arg0 = value;
    }

    /**
     * Gets the value of the arg1 property.
     * 
     * @return
     *     possible object is
     *     {@link TypeOfTransaction }
     *     
     */
    public TypeOfTransaction getArg1() {
        return arg1;
    }

    /**
     * Sets the value of the arg1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeOfTransaction }
     *     
     */
    public void setArg1(TypeOfTransaction value) {
        this.arg1 = value;
    }

    /**
     * Gets the value of the arg2 property.
     * 
     * @return
     *     possible object is
     *     {@link TransferRequestDTO }
     *     
     */
    public TransferRequestDTO getArg2() {
        return arg2;
    }

    /**
     * Sets the value of the arg2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransferRequestDTO }
     *     
     */
    public void setArg2(TransferRequestDTO value) {
        this.arg2 = value;
    }

}
