
package advcash.wsm;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the advcash.wsm package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ValidationSendMoneyToAdvcashCard_QNAME = new QName("http://wsm.advcash/", "validationSendMoneyToAdvcashCard");
    private final static QName _BadParametersException_QNAME = new QName("http://wsm.advcash/", "BadParametersException");
    private final static QName _SendMoneyToEcoinEU_QNAME = new QName("http://wsm.advcash/", "sendMoneyToEcoinEU");
    private final static QName _LimitPerMonthException_QNAME = new QName("http://wsm.advcash/", "LimitPerMonthException");
    private final static QName _WalletCurrencyIncorrectException_QNAME = new QName("http://wsm.advcash/", "WalletCurrencyIncorrectException");
    private final static QName _ValidateAccount_QNAME = new QName("http://wsm.advcash/", "validateAccount");
    private final static QName _ValidateAccountsResponse_QNAME = new QName("http://wsm.advcash/", "validateAccountsResponse");
    private final static QName _ValidateAccounts_QNAME = new QName("http://wsm.advcash/", "validateAccounts");
    private final static QName _SendMoneyToWexResponse_QNAME = new QName("http://wsm.advcash/", "sendMoneyToWexResponse");
    private final static QName _RegistrationException_QNAME = new QName("http://wsm.advcash/", "RegistrationException");
    private final static QName _Register_QNAME = new QName("http://wsm.advcash/", "register");
    private final static QName _ApiException_QNAME = new QName("http://wsm.advcash/", "ApiException");
    private final static QName _CodeIsNotValidException_QNAME = new QName("http://wsm.advcash/", "CodeIsNotValidException");
    private final static QName _FindTransaction_QNAME = new QName("http://wsm.advcash/", "findTransaction");
    private final static QName _WrongIpException_QNAME = new QName("http://wsm.advcash/", "WrongIpException");
    private final static QName _ValidationSendMoneyToEcoinEUResponse_QNAME = new QName("http://wsm.advcash/", "validationSendMoneyToEcoinEUResponse");
    private final static QName _ValidationSendMoneyToEmail_QNAME = new QName("http://wsm.advcash/", "validationSendMoneyToEmail");
    private final static QName _SendMoneyToBankCard_QNAME = new QName("http://wsm.advcash/", "sendMoneyToBankCard");
    private final static QName _ValidationSendMoneyToEcoinEU_QNAME = new QName("http://wsm.advcash/", "validationSendMoneyToEcoinEU");
    private final static QName _TransactionTemporaryNotAvailableException_QNAME = new QName("http://wsm.advcash/", "TransactionTemporaryNotAvailableException");
    private final static QName _ExchangeCurrencyException_QNAME = new QName("http://wsm.advcash/", "ExchangeCurrencyException");
    private final static QName _CardDoesNotExistException_QNAME = new QName("http://wsm.advcash/", "CardDoesNotExistException");
    private final static QName _WalletDoesNotExist_QNAME = new QName("http://wsm.advcash/", "WalletDoesNotExist");
    private final static QName _NotAuthException_QNAME = new QName("http://wsm.advcash/", "NotAuthException");
    private final static QName _WrongParamsException_QNAME = new QName("http://wsm.advcash/", "WrongParamsException");
    private final static QName _ValidateWithdrawalThroughExternalPaymentSystemResponse_QNAME = new QName("http://wsm.advcash/", "validateWithdrawalThroughExternalPaymentSystemResponse");
    private final static QName _DuplicateOrderIdException_QNAME = new QName("http://wsm.advcash/", "DuplicateOrderIdException");
    private final static QName _ValidationSendMoneyResponse_QNAME = new QName("http://wsm.advcash/", "validationSendMoneyResponse");
    private final static QName _MerchantDisabledException_QNAME = new QName("http://wsm.advcash/", "MerchantDisabledException");
    private final static QName _CurrencyExchangeResponse_QNAME = new QName("http://wsm.advcash/", "currencyExchangeResponse");
    private final static QName _ValidateCurrencyExchange_QNAME = new QName("http://wsm.advcash/", "validateCurrencyExchange");
    private final static QName _CardNumberIsNotValidException_QNAME = new QName("http://wsm.advcash/", "CardNumberIsNotValidException");
    private final static QName _SendMoneyResponse_QNAME = new QName("http://wsm.advcash/", "sendMoneyResponse");
    private final static QName _TransactionIsNotAvailableException_QNAME = new QName("http://wsm.advcash/", "TransactionIsNotAvailableException");
    private final static QName _NotAvailableDepositSystemException_QNAME = new QName("http://wsm.advcash/", "NotAvailableDepositSystemException");
    private final static QName _EmailTransferResponse_QNAME = new QName("http://wsm.advcash/", "emailTransferResponse");
    private final static QName _ValidateAdvcashCardTransferResponse_QNAME = new QName("http://wsm.advcash/", "validateAdvcashCardTransferResponse");
    private final static QName _LimitPerDayException_QNAME = new QName("http://wsm.advcash/", "LimitPerDayException");
    private final static QName _DatabaseException_QNAME = new QName("http://wsm.advcash/", "DatabaseException");
    private final static QName _SendMoneyToEcoinEUResponse_QNAME = new QName("http://wsm.advcash/", "sendMoneyToEcoinEUResponse");
    private final static QName _WithdrawalThroughExternalPaymentSystemResponse_QNAME = new QName("http://wsm.advcash/", "withdrawalThroughExternalPaymentSystemResponse");
    private final static QName _ValidationSendMoneyToWex_QNAME = new QName("http://wsm.advcash/", "validationSendMoneyToWex");
    private final static QName _MakeCurrencyExchangeResponse_QNAME = new QName("http://wsm.advcash/", "makeCurrencyExchangeResponse");
    private final static QName _ValidateAccountResponse_QNAME = new QName("http://wsm.advcash/", "validateAccountResponse");
    private final static QName _TransferBankCardResponse_QNAME = new QName("http://wsm.advcash/", "transferBankCardResponse");
    private final static QName _ValidationSendMoneyToBankCard_QNAME = new QName("http://wsm.advcash/", "validationSendMoneyToBankCard");
    private final static QName _SendMoneyToAdvcashCard_QNAME = new QName("http://wsm.advcash/", "sendMoneyToAdvcashCard");
    private final static QName _TransactionFailureException_QNAME = new QName("http://wsm.advcash/", "TransactionFailureException");
    private final static QName _CurrencyExchange_QNAME = new QName("http://wsm.advcash/", "currencyExchange");
    private final static QName _ValidationCurrencyExchangeResponse_QNAME = new QName("http://wsm.advcash/", "validationCurrencyExchangeResponse");
    private final static QName _TransferAdvcashCard_QNAME = new QName("http://wsm.advcash/", "transferAdvcashCard");
    private final static QName _SendMoneyToEmailResponse_QNAME = new QName("http://wsm.advcash/", "sendMoneyToEmailResponse");
    private final static QName _WithdrawalThroughExternalPaymentSystem_QNAME = new QName("http://wsm.advcash/", "withdrawalThroughExternalPaymentSystem");
    private final static QName _ValidateAdvcashCardTransfer_QNAME = new QName("http://wsm.advcash/", "validateAdvcashCardTransfer");
    private final static QName _FindTransactionResponse_QNAME = new QName("http://wsm.advcash/", "findTransactionResponse");
    private final static QName _ValidationSendMoneyToEmailResponse_QNAME = new QName("http://wsm.advcash/", "validationSendMoneyToEmailResponse");
    private final static QName _ValidateTransfer_QNAME = new QName("http://wsm.advcash/", "validateTransfer");
    private final static QName _MakeTransferResponse_QNAME = new QName("http://wsm.advcash/", "makeTransferResponse");
    private final static QName _SendMoneyToWex_QNAME = new QName("http://wsm.advcash/", "sendMoneyToWex");
    private final static QName _InternalException_QNAME = new QName("http://wsm.advcash/", "InternalException");
    private final static QName _ValidateTransferResponse_QNAME = new QName("http://wsm.advcash/", "validateTransferResponse");
    private final static QName _WrongEmailException_QNAME = new QName("http://wsm.advcash/", "WrongEmailException");
    private final static QName _SendMoneyToExmo_QNAME = new QName("http://wsm.advcash/", "sendMoneyToExmo");
    private final static QName _GetBalancesResponse_QNAME = new QName("http://wsm.advcash/", "getBalancesResponse");
    private final static QName _MakeCurrencyExchange_QNAME = new QName("http://wsm.advcash/", "makeCurrencyExchange");
    private final static QName _RegisterResponse_QNAME = new QName("http://wsm.advcash/", "registerResponse");
    private final static QName _CreateBitcoinInvoiceResponse_QNAME = new QName("http://wsm.advcash/", "createBitcoinInvoiceResponse");
    private final static QName _TransferBankCard_QNAME = new QName("http://wsm.advcash/", "transferBankCard");
    private final static QName _LimitPerCardPerDayException_QNAME = new QName("http://wsm.advcash/", "LimitPerCardPerDayException");
    private final static QName _ValidationSendMoneyToExmoResponse_QNAME = new QName("http://wsm.advcash/", "validationSendMoneyToExmoResponse");
    private final static QName _MakeTransfer_QNAME = new QName("http://wsm.advcash/", "makeTransfer");
    private final static QName _HistoryResponse_QNAME = new QName("http://wsm.advcash/", "historyResponse");
    private final static QName _ValidationSendMoneyToEcurrencyResponse_QNAME = new QName("http://wsm.advcash/", "validationSendMoneyToEcurrencyResponse");
    private final static QName _ValidateWithdrawalThroughExternalPaymentSystem_QNAME = new QName("http://wsm.advcash/", "validateWithdrawalThroughExternalPaymentSystem");
    private final static QName _ValidateEmailTransfer_QNAME = new QName("http://wsm.advcash/", "validateEmailTransfer");
    private final static QName _LimitsException_QNAME = new QName("http://wsm.advcash/", "LimitsException");
    private final static QName _CheckCurrencyExchange_QNAME = new QName("http://wsm.advcash/", "checkCurrencyExchange");
    private final static QName _CreateBitcoinInvoice_QNAME = new QName("http://wsm.advcash/", "createBitcoinInvoice");
    private final static QName _NotEnoughMoneyException_QNAME = new QName("http://wsm.advcash/", "NotEnoughMoneyException");
    private final static QName _ValidateBankCardTransferResponse_QNAME = new QName("http://wsm.advcash/", "validateBankCardTransferResponse");
    private final static QName _LimitPerTransactionException_QNAME = new QName("http://wsm.advcash/", "LimitPerTransactionException");
    private final static QName _ValidationCurrencyExchange_QNAME = new QName("http://wsm.advcash/", "validationCurrencyExchange");
    private final static QName _History_QNAME = new QName("http://wsm.advcash/", "history");
    private final static QName _UserBlockedException_QNAME = new QName("http://wsm.advcash/", "UserBlockedException");
    private final static QName _ValidationSendMoneyToAdvcashCardResponse_QNAME = new QName("http://wsm.advcash/", "validationSendMoneyToAdvcashCardResponse");
    private final static QName _JAXBException_QNAME = new QName("http://wsm.advcash/", "JAXBException");
    private final static QName _CallRestrictionException_QNAME = new QName("http://wsm.advcash/", "CallRestrictionException");
    private final static QName _SendMoneyToBankCardResponse_QNAME = new QName("http://wsm.advcash/", "sendMoneyToBankCardResponse");
    private final static QName _NotSupportedBankBinException_QNAME = new QName("http://wsm.advcash/", "NotSupportedBankBinException");
    private final static QName _SendMoneyToExmoResponse_QNAME = new QName("http://wsm.advcash/", "sendMoneyToExmoResponse");
    private final static QName _LifetimeLimitException_QNAME = new QName("http://wsm.advcash/", "LifetimeLimitException");
    private final static QName _CheckCurrencyExchangeResponse_QNAME = new QName("http://wsm.advcash/", "checkCurrencyExchangeResponse");
    private final static QName _ValidateCurrencyExchangeResponse_QNAME = new QName("http://wsm.advcash/", "validateCurrencyExchangeResponse");
    private final static QName _SendMoneyToEcurrencyResponse_QNAME = new QName("http://wsm.advcash/", "sendMoneyToEcurrencyResponse");
    private final static QName _SendMoneyToEmail_QNAME = new QName("http://wsm.advcash/", "sendMoneyToEmail");
    private final static QName _SendMoney_QNAME = new QName("http://wsm.advcash/", "sendMoney");
    private final static QName _ValidationSendMoneyToEcurrency_QNAME = new QName("http://wsm.advcash/", "validationSendMoneyToEcurrency");
    private final static QName _CardIsNotActiveException_QNAME = new QName("http://wsm.advcash/", "CardIsNotActiveException");
    private final static QName _SendMoneyToAdvcashCardResponse_QNAME = new QName("http://wsm.advcash/", "sendMoneyToAdvcashCardResponse");
    private final static QName _AdditionalDataRequiredException_QNAME = new QName("http://wsm.advcash/", "AdditionalDataRequiredException");
    private final static QName _SendMoneyToEcurrency_QNAME = new QName("http://wsm.advcash/", "sendMoneyToEcurrency");
    private final static QName _UserDoesNotExistException_QNAME = new QName("http://wsm.advcash/", "UserDoesNotExistException");
    private final static QName _ValidateBankCardTransfer_QNAME = new QName("http://wsm.advcash/", "validateBankCardTransfer");
    private final static QName _ValidateEmailTransferResponse_QNAME = new QName("http://wsm.advcash/", "validateEmailTransferResponse");
    private final static QName _EmailTransfer_QNAME = new QName("http://wsm.advcash/", "emailTransfer");
    private final static QName _ValidationSendMoneyToWexResponse_QNAME = new QName("http://wsm.advcash/", "validationSendMoneyToWexResponse");
    private final static QName _ValidationSendMoneyToExmo_QNAME = new QName("http://wsm.advcash/", "validationSendMoneyToExmo");
    private final static QName _AccessDeniedException_QNAME = new QName("http://wsm.advcash/", "AccessDeniedException");
    private final static QName _ValidationSendMoneyToBankCardResponse_QNAME = new QName("http://wsm.advcash/", "validationSendMoneyToBankCardResponse");
    private final static QName _EmailAlreadyExistException_QNAME = new QName("http://wsm.advcash/", "EmailAlreadyExistException");
    private final static QName _TransferAdvcashCardResponse_QNAME = new QName("http://wsm.advcash/", "transferAdvcashCardResponse");
    private final static QName _NotSupportedCountryException_QNAME = new QName("http://wsm.advcash/", "NotSupportedCountryException");
    private final static QName _NotEnoughMoneyApiException_QNAME = new QName("http://wsm.advcash/", "NotEnoughMoneyApiException");
    private final static QName _ValidationSendMoney_QNAME = new QName("http://wsm.advcash/", "validationSendMoney");
    private final static QName _GetBalances_QNAME = new QName("http://wsm.advcash/", "getBalances");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: advcash.wsm
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ValidateAdvcashCardTransfer }
     * 
     */
    public ValidateAdvcashCardTransfer createValidateAdvcashCardTransfer() {
        return new ValidateAdvcashCardTransfer();
    }

    /**
     * Create an instance of {@link WithdrawalThroughExternalPaymentSystem }
     * 
     */
    public WithdrawalThroughExternalPaymentSystem createWithdrawalThroughExternalPaymentSystem() {
        return new WithdrawalThroughExternalPaymentSystem();
    }

    /**
     * Create an instance of {@link SendMoneyToEmailResponse }
     * 
     */
    public SendMoneyToEmailResponse createSendMoneyToEmailResponse() {
        return new SendMoneyToEmailResponse();
    }

    /**
     * Create an instance of {@link TransferAdvcashCard }
     * 
     */
    public TransferAdvcashCard createTransferAdvcashCard() {
        return new TransferAdvcashCard();
    }

    /**
     * Create an instance of {@link CurrencyExchange }
     * 
     */
    public CurrencyExchange createCurrencyExchange() {
        return new CurrencyExchange();
    }

    /**
     * Create an instance of {@link ValidationCurrencyExchangeResponse }
     * 
     */
    public ValidationCurrencyExchangeResponse createValidationCurrencyExchangeResponse() {
        return new ValidationCurrencyExchangeResponse();
    }

    /**
     * Create an instance of {@link SendMoneyToWex }
     * 
     */
    public SendMoneyToWex createSendMoneyToWex() {
        return new SendMoneyToWex();
    }

    /**
     * Create an instance of {@link MakeTransferResponse }
     * 
     */
    public MakeTransferResponse createMakeTransferResponse() {
        return new MakeTransferResponse();
    }

    /**
     * Create an instance of {@link ValidateTransfer }
     * 
     */
    public ValidateTransfer createValidateTransfer() {
        return new ValidateTransfer();
    }

    /**
     * Create an instance of {@link ValidationSendMoneyToEmailResponse }
     * 
     */
    public ValidationSendMoneyToEmailResponse createValidationSendMoneyToEmailResponse() {
        return new ValidationSendMoneyToEmailResponse();
    }

    /**
     * Create an instance of {@link FindTransactionResponse }
     * 
     */
    public FindTransactionResponse createFindTransactionResponse() {
        return new FindTransactionResponse();
    }

    /**
     * Create an instance of {@link EmailTransferResponse }
     * 
     */
    public EmailTransferResponse createEmailTransferResponse() {
        return new EmailTransferResponse();
    }

    /**
     * Create an instance of {@link TransactionIsNotAvailableException }
     * 
     */
    public TransactionIsNotAvailableException createTransactionIsNotAvailableException() {
        return new TransactionIsNotAvailableException();
    }

    /**
     * Create an instance of {@link NotAvailableDepositSystemException }
     * 
     */
    public NotAvailableDepositSystemException createNotAvailableDepositSystemException() {
        return new NotAvailableDepositSystemException();
    }

    /**
     * Create an instance of {@link SendMoneyResponse }
     * 
     */
    public SendMoneyResponse createSendMoneyResponse() {
        return new SendMoneyResponse();
    }

    /**
     * Create an instance of {@link CurrencyExchangeResponse }
     * 
     */
    public CurrencyExchangeResponse createCurrencyExchangeResponse() {
        return new CurrencyExchangeResponse();
    }

    /**
     * Create an instance of {@link ValidateCurrencyExchange }
     * 
     */
    public ValidateCurrencyExchange createValidateCurrencyExchange() {
        return new ValidateCurrencyExchange();
    }

    /**
     * Create an instance of {@link CardNumberIsNotValidException }
     * 
     */
    public CardNumberIsNotValidException createCardNumberIsNotValidException() {
        return new CardNumberIsNotValidException();
    }

    /**
     * Create an instance of {@link ValidationSendMoneyResponse }
     * 
     */
    public ValidationSendMoneyResponse createValidationSendMoneyResponse() {
        return new ValidationSendMoneyResponse();
    }

    /**
     * Create an instance of {@link MerchantDisabledException }
     * 
     */
    public MerchantDisabledException createMerchantDisabledException() {
        return new MerchantDisabledException();
    }

    /**
     * Create an instance of {@link DuplicateOrderIdException }
     * 
     */
    public DuplicateOrderIdException createDuplicateOrderIdException() {
        return new DuplicateOrderIdException();
    }

    /**
     * Create an instance of {@link ValidateWithdrawalThroughExternalPaymentSystemResponse }
     * 
     */
    public ValidateWithdrawalThroughExternalPaymentSystemResponse createValidateWithdrawalThroughExternalPaymentSystemResponse() {
        return new ValidateWithdrawalThroughExternalPaymentSystemResponse();
    }

    /**
     * Create an instance of {@link WrongParamsException }
     * 
     */
    public WrongParamsException createWrongParamsException() {
        return new WrongParamsException();
    }

    /**
     * Create an instance of {@link TransactionFailureException }
     * 
     */
    public TransactionFailureException createTransactionFailureException() {
        return new TransactionFailureException();
    }

    /**
     * Create an instance of {@link SendMoneyToAdvcashCard }
     * 
     */
    public SendMoneyToAdvcashCard createSendMoneyToAdvcashCard() {
        return new SendMoneyToAdvcashCard();
    }

    /**
     * Create an instance of {@link ValidationSendMoneyToBankCard }
     * 
     */
    public ValidationSendMoneyToBankCard createValidationSendMoneyToBankCard() {
        return new ValidationSendMoneyToBankCard();
    }

    /**
     * Create an instance of {@link TransferBankCardResponse }
     * 
     */
    public TransferBankCardResponse createTransferBankCardResponse() {
        return new TransferBankCardResponse();
    }

    /**
     * Create an instance of {@link ValidateAccountResponse }
     * 
     */
    public ValidateAccountResponse createValidateAccountResponse() {
        return new ValidateAccountResponse();
    }

    /**
     * Create an instance of {@link MakeCurrencyExchangeResponse }
     * 
     */
    public MakeCurrencyExchangeResponse createMakeCurrencyExchangeResponse() {
        return new MakeCurrencyExchangeResponse();
    }

    /**
     * Create an instance of {@link ValidationSendMoneyToWex }
     * 
     */
    public ValidationSendMoneyToWex createValidationSendMoneyToWex() {
        return new ValidationSendMoneyToWex();
    }

    /**
     * Create an instance of {@link SendMoneyToEcoinEUResponse }
     * 
     */
    public SendMoneyToEcoinEUResponse createSendMoneyToEcoinEUResponse() {
        return new SendMoneyToEcoinEUResponse();
    }

    /**
     * Create an instance of {@link WithdrawalThroughExternalPaymentSystemResponse }
     * 
     */
    public WithdrawalThroughExternalPaymentSystemResponse createWithdrawalThroughExternalPaymentSystemResponse() {
        return new WithdrawalThroughExternalPaymentSystemResponse();
    }

    /**
     * Create an instance of {@link DatabaseException }
     * 
     */
    public DatabaseException createDatabaseException() {
        return new DatabaseException();
    }

    /**
     * Create an instance of {@link ValidateAdvcashCardTransferResponse }
     * 
     */
    public ValidateAdvcashCardTransferResponse createValidateAdvcashCardTransferResponse() {
        return new ValidateAdvcashCardTransferResponse();
    }

    /**
     * Create an instance of {@link LimitPerDayException }
     * 
     */
    public LimitPerDayException createLimitPerDayException() {
        return new LimitPerDayException();
    }

    /**
     * Create an instance of {@link TransactionTemporaryNotAvailableException }
     * 
     */
    public TransactionTemporaryNotAvailableException createTransactionTemporaryNotAvailableException() {
        return new TransactionTemporaryNotAvailableException();
    }

    /**
     * Create an instance of {@link SendMoneyToBankCard }
     * 
     */
    public SendMoneyToBankCard createSendMoneyToBankCard() {
        return new SendMoneyToBankCard();
    }

    /**
     * Create an instance of {@link ValidationSendMoneyToEcoinEU }
     * 
     */
    public ValidationSendMoneyToEcoinEU createValidationSendMoneyToEcoinEU() {
        return new ValidationSendMoneyToEcoinEU();
    }

    /**
     * Create an instance of {@link ValidationSendMoneyToEmail }
     * 
     */
    public ValidationSendMoneyToEmail createValidationSendMoneyToEmail() {
        return new ValidationSendMoneyToEmail();
    }

    /**
     * Create an instance of {@link ValidationSendMoneyToEcoinEUResponse }
     * 
     */
    public ValidationSendMoneyToEcoinEUResponse createValidationSendMoneyToEcoinEUResponse() {
        return new ValidationSendMoneyToEcoinEUResponse();
    }

    /**
     * Create an instance of {@link WrongIpException }
     * 
     */
    public WrongIpException createWrongIpException() {
        return new WrongIpException();
    }

    /**
     * Create an instance of {@link NotAuthException }
     * 
     */
    public NotAuthException createNotAuthException() {
        return new NotAuthException();
    }

    /**
     * Create an instance of {@link WalletDoesNotExist }
     * 
     */
    public WalletDoesNotExist createWalletDoesNotExist() {
        return new WalletDoesNotExist();
    }

    /**
     * Create an instance of {@link CardDoesNotExistException }
     * 
     */
    public CardDoesNotExistException createCardDoesNotExistException() {
        return new CardDoesNotExistException();
    }

    /**
     * Create an instance of {@link ExchangeCurrencyException }
     * 
     */
    public ExchangeCurrencyException createExchangeCurrencyException() {
        return new ExchangeCurrencyException();
    }

    /**
     * Create an instance of {@link RegistrationException }
     * 
     */
    public RegistrationException createRegistrationException() {
        return new RegistrationException();
    }

    /**
     * Create an instance of {@link SendMoneyToWexResponse }
     * 
     */
    public SendMoneyToWexResponse createSendMoneyToWexResponse() {
        return new SendMoneyToWexResponse();
    }

    /**
     * Create an instance of {@link ValidateAccounts }
     * 
     */
    public ValidateAccounts createValidateAccounts() {
        return new ValidateAccounts();
    }

    /**
     * Create an instance of {@link ValidateAccount }
     * 
     */
    public ValidateAccount createValidateAccount() {
        return new ValidateAccount();
    }

    /**
     * Create an instance of {@link ValidateAccountsResponse }
     * 
     */
    public ValidateAccountsResponse createValidateAccountsResponse() {
        return new ValidateAccountsResponse();
    }

    /**
     * Create an instance of {@link WalletCurrencyIncorrectException }
     * 
     */
    public WalletCurrencyIncorrectException createWalletCurrencyIncorrectException() {
        return new WalletCurrencyIncorrectException();
    }

    /**
     * Create an instance of {@link SendMoneyToEcoinEU }
     * 
     */
    public SendMoneyToEcoinEU createSendMoneyToEcoinEU() {
        return new SendMoneyToEcoinEU();
    }

    /**
     * Create an instance of {@link LimitPerMonthException }
     * 
     */
    public LimitPerMonthException createLimitPerMonthException() {
        return new LimitPerMonthException();
    }

    /**
     * Create an instance of {@link ValidationSendMoneyToAdvcashCard }
     * 
     */
    public ValidationSendMoneyToAdvcashCard createValidationSendMoneyToAdvcashCard() {
        return new ValidationSendMoneyToAdvcashCard();
    }

    /**
     * Create an instance of {@link BadParametersException }
     * 
     */
    public BadParametersException createBadParametersException() {
        return new BadParametersException();
    }

    /**
     * Create an instance of {@link FindTransaction }
     * 
     */
    public FindTransaction createFindTransaction() {
        return new FindTransaction();
    }

    /**
     * Create an instance of {@link CodeIsNotValidException }
     * 
     */
    public CodeIsNotValidException createCodeIsNotValidException() {
        return new CodeIsNotValidException();
    }

    /**
     * Create an instance of {@link Register }
     * 
     */
    public Register createRegister() {
        return new Register();
    }

    /**
     * Create an instance of {@link ApiException }
     * 
     */
    public ApiException createApiException() {
        return new ApiException();
    }

    /**
     * Create an instance of {@link ValidationSendMoneyToExmo }
     * 
     */
    public ValidationSendMoneyToExmo createValidationSendMoneyToExmo() {
        return new ValidationSendMoneyToExmo();
    }

    /**
     * Create an instance of {@link AccessDeniedException }
     * 
     */
    public AccessDeniedException createAccessDeniedException() {
        return new AccessDeniedException();
    }

    /**
     * Create an instance of {@link ValidationSendMoneyToWexResponse }
     * 
     */
    public ValidationSendMoneyToWexResponse createValidationSendMoneyToWexResponse() {
        return new ValidationSendMoneyToWexResponse();
    }

    /**
     * Create an instance of {@link EmailTransfer }
     * 
     */
    public EmailTransfer createEmailTransfer() {
        return new EmailTransfer();
    }

    /**
     * Create an instance of {@link ValidateBankCardTransfer }
     * 
     */
    public ValidateBankCardTransfer createValidateBankCardTransfer() {
        return new ValidateBankCardTransfer();
    }

    /**
     * Create an instance of {@link ValidateEmailTransferResponse }
     * 
     */
    public ValidateEmailTransferResponse createValidateEmailTransferResponse() {
        return new ValidateEmailTransferResponse();
    }

    /**
     * Create an instance of {@link UserDoesNotExistException }
     * 
     */
    public UserDoesNotExistException createUserDoesNotExistException() {
        return new UserDoesNotExistException();
    }

    /**
     * Create an instance of {@link SendMoneyToEcurrency }
     * 
     */
    public SendMoneyToEcurrency createSendMoneyToEcurrency() {
        return new SendMoneyToEcurrency();
    }

    /**
     * Create an instance of {@link SendMoneyToAdvcashCardResponse }
     * 
     */
    public SendMoneyToAdvcashCardResponse createSendMoneyToAdvcashCardResponse() {
        return new SendMoneyToAdvcashCardResponse();
    }

    /**
     * Create an instance of {@link AdditionalDataRequiredException }
     * 
     */
    public AdditionalDataRequiredException createAdditionalDataRequiredException() {
        return new AdditionalDataRequiredException();
    }

    /**
     * Create an instance of {@link SendMoney }
     * 
     */
    public SendMoney createSendMoney() {
        return new SendMoney();
    }

    /**
     * Create an instance of {@link ValidationSendMoneyToEcurrency }
     * 
     */
    public ValidationSendMoneyToEcurrency createValidationSendMoneyToEcurrency() {
        return new ValidationSendMoneyToEcurrency();
    }

    /**
     * Create an instance of {@link CardIsNotActiveException }
     * 
     */
    public CardIsNotActiveException createCardIsNotActiveException() {
        return new CardIsNotActiveException();
    }

    /**
     * Create an instance of {@link GetBalances }
     * 
     */
    public GetBalances createGetBalances() {
        return new GetBalances();
    }

    /**
     * Create an instance of {@link ValidationSendMoney }
     * 
     */
    public ValidationSendMoney createValidationSendMoney() {
        return new ValidationSendMoney();
    }

    /**
     * Create an instance of {@link NotEnoughMoneyApiException }
     * 
     */
    public NotEnoughMoneyApiException createNotEnoughMoneyApiException() {
        return new NotEnoughMoneyApiException();
    }

    /**
     * Create an instance of {@link NotSupportedCountryException }
     * 
     */
    public NotSupportedCountryException createNotSupportedCountryException() {
        return new NotSupportedCountryException();
    }

    /**
     * Create an instance of {@link TransferAdvcashCardResponse }
     * 
     */
    public TransferAdvcashCardResponse createTransferAdvcashCardResponse() {
        return new TransferAdvcashCardResponse();
    }

    /**
     * Create an instance of {@link ValidationSendMoneyToBankCardResponse }
     * 
     */
    public ValidationSendMoneyToBankCardResponse createValidationSendMoneyToBankCardResponse() {
        return new ValidationSendMoneyToBankCardResponse();
    }

    /**
     * Create an instance of {@link EmailAlreadyExistException }
     * 
     */
    public EmailAlreadyExistException createEmailAlreadyExistException() {
        return new EmailAlreadyExistException();
    }

    /**
     * Create an instance of {@link SendMoneyToExmoResponse }
     * 
     */
    public SendMoneyToExmoResponse createSendMoneyToExmoResponse() {
        return new SendMoneyToExmoResponse();
    }

    /**
     * Create an instance of {@link NotSupportedBankBinException }
     * 
     */
    public NotSupportedBankBinException createNotSupportedBankBinException() {
        return new NotSupportedBankBinException();
    }

    /**
     * Create an instance of {@link SendMoneyToBankCardResponse }
     * 
     */
    public SendMoneyToBankCardResponse createSendMoneyToBankCardResponse() {
        return new SendMoneyToBankCardResponse();
    }

    /**
     * Create an instance of {@link CallRestrictionException }
     * 
     */
    public CallRestrictionException createCallRestrictionException() {
        return new CallRestrictionException();
    }

    /**
     * Create an instance of {@link JAXBException }
     * 
     */
    public JAXBException createJAXBException() {
        return new JAXBException();
    }

    /**
     * Create an instance of {@link ValidationSendMoneyToAdvcashCardResponse }
     * 
     */
    public ValidationSendMoneyToAdvcashCardResponse createValidationSendMoneyToAdvcashCardResponse() {
        return new ValidationSendMoneyToAdvcashCardResponse();
    }

    /**
     * Create an instance of {@link UserBlockedException }
     * 
     */
    public UserBlockedException createUserBlockedException() {
        return new UserBlockedException();
    }

    /**
     * Create an instance of {@link History }
     * 
     */
    public History createHistory() {
        return new History();
    }

    /**
     * Create an instance of {@link ValidationCurrencyExchange }
     * 
     */
    public ValidationCurrencyExchange createValidationCurrencyExchange() {
        return new ValidationCurrencyExchange();
    }

    /**
     * Create an instance of {@link ValidateBankCardTransferResponse }
     * 
     */
    public ValidateBankCardTransferResponse createValidateBankCardTransferResponse() {
        return new ValidateBankCardTransferResponse();
    }

    /**
     * Create an instance of {@link LimitPerTransactionException }
     * 
     */
    public LimitPerTransactionException createLimitPerTransactionException() {
        return new LimitPerTransactionException();
    }

    /**
     * Create an instance of {@link SendMoneyToEmail }
     * 
     */
    public SendMoneyToEmail createSendMoneyToEmail() {
        return new SendMoneyToEmail();
    }

    /**
     * Create an instance of {@link SendMoneyToEcurrencyResponse }
     * 
     */
    public SendMoneyToEcurrencyResponse createSendMoneyToEcurrencyResponse() {
        return new SendMoneyToEcurrencyResponse();
    }

    /**
     * Create an instance of {@link CheckCurrencyExchangeResponse }
     * 
     */
    public CheckCurrencyExchangeResponse createCheckCurrencyExchangeResponse() {
        return new CheckCurrencyExchangeResponse();
    }

    /**
     * Create an instance of {@link ValidateCurrencyExchangeResponse }
     * 
     */
    public ValidateCurrencyExchangeResponse createValidateCurrencyExchangeResponse() {
        return new ValidateCurrencyExchangeResponse();
    }

    /**
     * Create an instance of {@link LifetimeLimitException }
     * 
     */
    public LifetimeLimitException createLifetimeLimitException() {
        return new LifetimeLimitException();
    }

    /**
     * Create an instance of {@link HistoryResponse }
     * 
     */
    public HistoryResponse createHistoryResponse() {
        return new HistoryResponse();
    }

    /**
     * Create an instance of {@link MakeTransfer }
     * 
     */
    public MakeTransfer createMakeTransfer() {
        return new MakeTransfer();
    }

    /**
     * Create an instance of {@link ValidationSendMoneyToExmoResponse }
     * 
     */
    public ValidationSendMoneyToExmoResponse createValidationSendMoneyToExmoResponse() {
        return new ValidationSendMoneyToExmoResponse();
    }

    /**
     * Create an instance of {@link NotEnoughMoneyException }
     * 
     */
    public NotEnoughMoneyException createNotEnoughMoneyException() {
        return new NotEnoughMoneyException();
    }

    /**
     * Create an instance of {@link CheckCurrencyExchange }
     * 
     */
    public CheckCurrencyExchange createCheckCurrencyExchange() {
        return new CheckCurrencyExchange();
    }

    /**
     * Create an instance of {@link CreateBitcoinInvoice }
     * 
     */
    public CreateBitcoinInvoice createCreateBitcoinInvoice() {
        return new CreateBitcoinInvoice();
    }

    /**
     * Create an instance of {@link ValidateEmailTransfer }
     * 
     */
    public ValidateEmailTransfer createValidateEmailTransfer() {
        return new ValidateEmailTransfer();
    }

    /**
     * Create an instance of {@link LimitsException }
     * 
     */
    public LimitsException createLimitsException() {
        return new LimitsException();
    }

    /**
     * Create an instance of {@link ValidateWithdrawalThroughExternalPaymentSystem }
     * 
     */
    public ValidateWithdrawalThroughExternalPaymentSystem createValidateWithdrawalThroughExternalPaymentSystem() {
        return new ValidateWithdrawalThroughExternalPaymentSystem();
    }

    /**
     * Create an instance of {@link ValidationSendMoneyToEcurrencyResponse }
     * 
     */
    public ValidationSendMoneyToEcurrencyResponse createValidationSendMoneyToEcurrencyResponse() {
        return new ValidationSendMoneyToEcurrencyResponse();
    }

    /**
     * Create an instance of {@link SendMoneyToExmo }
     * 
     */
    public SendMoneyToExmo createSendMoneyToExmo() {
        return new SendMoneyToExmo();
    }

    /**
     * Create an instance of {@link WrongEmailException }
     * 
     */
    public WrongEmailException createWrongEmailException() {
        return new WrongEmailException();
    }

    /**
     * Create an instance of {@link ValidateTransferResponse }
     * 
     */
    public ValidateTransferResponse createValidateTransferResponse() {
        return new ValidateTransferResponse();
    }

    /**
     * Create an instance of {@link InternalException }
     * 
     */
    public InternalException createInternalException() {
        return new InternalException();
    }

    /**
     * Create an instance of {@link TransferBankCard }
     * 
     */
    public TransferBankCard createTransferBankCard() {
        return new TransferBankCard();
    }

    /**
     * Create an instance of {@link LimitPerCardPerDayException }
     * 
     */
    public LimitPerCardPerDayException createLimitPerCardPerDayException() {
        return new LimitPerCardPerDayException();
    }

    /**
     * Create an instance of {@link CreateBitcoinInvoiceResponse }
     * 
     */
    public CreateBitcoinInvoiceResponse createCreateBitcoinInvoiceResponse() {
        return new CreateBitcoinInvoiceResponse();
    }

    /**
     * Create an instance of {@link GetBalancesResponse }
     * 
     */
    public GetBalancesResponse createGetBalancesResponse() {
        return new GetBalancesResponse();
    }

    /**
     * Create an instance of {@link MakeCurrencyExchange }
     * 
     */
    public MakeCurrencyExchange createMakeCurrencyExchange() {
        return new MakeCurrencyExchange();
    }

    /**
     * Create an instance of {@link RegisterResponse }
     * 
     */
    public RegisterResponse createRegisterResponse() {
        return new RegisterResponse();
    }

    /**
     * Create an instance of {@link TransferRequestDTO }
     * 
     */
    public TransferRequestDTO createTransferRequestDTO() {
        return new TransferRequestDTO();
    }

    /**
     * Create an instance of {@link BankCardTransferRequest }
     * 
     */
    public BankCardTransferRequest createBankCardTransferRequest() {
        return new BankCardTransferRequest();
    }

    /**
     * Create an instance of {@link CreateBitcoinInvoiceResult }
     * 
     */
    public CreateBitcoinInvoiceResult createCreateBitcoinInvoiceResult() {
        return new CreateBitcoinInvoiceResult();
    }

    /**
     * Create an instance of {@link OutcomingTransactionDTO }
     * 
     */
    public OutcomingTransactionDTO createOutcomingTransactionDTO() {
        return new OutcomingTransactionDTO();
    }

    /**
     * Create an instance of {@link WithdrawToEcurrencyRequest }
     * 
     */
    public WithdrawToEcurrencyRequest createWithdrawToEcurrencyRequest() {
        return new WithdrawToEcurrencyRequest();
    }

    /**
     * Create an instance of {@link EmailTransferRequestDTO }
     * 
     */
    public EmailTransferRequestDTO createEmailTransferRequestDTO() {
        return new EmailTransferRequestDTO();
    }

    /**
     * Create an instance of {@link AuthDTO }
     * 
     */
    public AuthDTO createAuthDTO() {
        return new AuthDTO();
    }

    /**
     * Create an instance of {@link SendMoneyToExmoResultHolder }
     * 
     */
    public SendMoneyToExmoResultHolder createSendMoneyToExmoResultHolder() {
        return new SendMoneyToExmoResultHolder();
    }

    /**
     * Create an instance of {@link WithdrawalThroughExternalPaymentSystemRequestDTO }
     * 
     */
    public WithdrawalThroughExternalPaymentSystemRequestDTO createWithdrawalThroughExternalPaymentSystemRequestDTO() {
        return new WithdrawalThroughExternalPaymentSystemRequestDTO();
    }

    /**
     * Create an instance of {@link CreateBitcoinInvoiceRequest }
     * 
     */
    public CreateBitcoinInvoiceRequest createCreateBitcoinInvoiceRequest() {
        return new CreateBitcoinInvoiceRequest();
    }

    /**
     * Create an instance of {@link MerchantAPITransactionFilter }
     * 
     */
    public MerchantAPITransactionFilter createMerchantAPITransactionFilter() {
        return new MerchantAPITransactionFilter();
    }

    /**
     * Create an instance of {@link ValidateAccountRequestDTO }
     * 
     */
    public ValidateAccountRequestDTO createValidateAccountRequestDTO() {
        return new ValidateAccountRequestDTO();
    }

    /**
     * Create an instance of {@link CurrencyExchangeRequest }
     * 
     */
    public CurrencyExchangeRequest createCurrencyExchangeRequest() {
        return new CurrencyExchangeRequest();
    }

    /**
     * Create an instance of {@link CheckCurrencyExchangeResultHolder }
     * 
     */
    public CheckCurrencyExchangeResultHolder createCheckCurrencyExchangeResultHolder() {
        return new CheckCurrencyExchangeResultHolder();
    }

    /**
     * Create an instance of {@link RegistrationRequest }
     * 
     */
    public RegistrationRequest createRegistrationRequest() {
        return new RegistrationRequest();
    }

    /**
     * Create an instance of {@link AdvcashCardTransferRequestDTO }
     * 
     */
    public AdvcashCardTransferRequestDTO createAdvcashCardTransferRequestDTO() {
        return new AdvcashCardTransferRequestDTO();
    }

    /**
     * Create an instance of {@link CheckCurrencyExchangeRequest }
     * 
     */
    public CheckCurrencyExchangeRequest createCheckCurrencyExchangeRequest() {
        return new CheckCurrencyExchangeRequest();
    }

    /**
     * Create an instance of {@link SendMoneyToEcoinEUResultHolder }
     * 
     */
    public SendMoneyToEcoinEUResultHolder createSendMoneyToEcoinEUResultHolder() {
        return new SendMoneyToEcoinEUResultHolder();
    }

    /**
     * Create an instance of {@link ValidateAccountResultDTO }
     * 
     */
    public ValidateAccountResultDTO createValidateAccountResultDTO() {
        return new ValidateAccountResultDTO();
    }

    /**
     * Create an instance of {@link WalletBalanceDTO }
     * 
     */
    public WalletBalanceDTO createWalletBalanceDTO() {
        return new WalletBalanceDTO();
    }

    /**
     * Create an instance of {@link SendMoneyToWexResultHolder }
     * 
     */
    public SendMoneyToWexResultHolder createSendMoneyToWexResultHolder() {
        return new SendMoneyToWexResultHolder();
    }

    /**
     * Create an instance of {@link AccountPresentDTO }
     * 
     */
    public AccountPresentDTO createAccountPresentDTO() {
        return new AccountPresentDTO();
    }

    /**
     * Create an instance of {@link AdvcashCardTransferRequest }
     * 
     */
    public AdvcashCardTransferRequest createAdvcashCardTransferRequest() {
        return new AdvcashCardTransferRequest();
    }

    /**
     * Create an instance of {@link BankCardTransferRequestDTO }
     * 
     */
    public BankCardTransferRequestDTO createBankCardTransferRequestDTO() {
        return new BankCardTransferRequestDTO();
    }

    /**
     * Create an instance of {@link SendMoneyRequest }
     * 
     */
    public SendMoneyRequest createSendMoneyRequest() {
        return new SendMoneyRequest();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ValidationSendMoneyToAdvcashCard }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "validationSendMoneyToAdvcashCard")
    public JAXBElement<ValidationSendMoneyToAdvcashCard> createValidationSendMoneyToAdvcashCard(ValidationSendMoneyToAdvcashCard value) {
        return new JAXBElement<ValidationSendMoneyToAdvcashCard>(_ValidationSendMoneyToAdvcashCard_QNAME, ValidationSendMoneyToAdvcashCard.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BadParametersException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "BadParametersException")
    public JAXBElement<BadParametersException> createBadParametersException(BadParametersException value) {
        return new JAXBElement<BadParametersException>(_BadParametersException_QNAME, BadParametersException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SendMoneyToEcoinEU }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "sendMoneyToEcoinEU")
    public JAXBElement<SendMoneyToEcoinEU> createSendMoneyToEcoinEU(SendMoneyToEcoinEU value) {
        return new JAXBElement<SendMoneyToEcoinEU>(_SendMoneyToEcoinEU_QNAME, SendMoneyToEcoinEU.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LimitPerMonthException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "LimitPerMonthException")
    public JAXBElement<LimitPerMonthException> createLimitPerMonthException(LimitPerMonthException value) {
        return new JAXBElement<LimitPerMonthException>(_LimitPerMonthException_QNAME, LimitPerMonthException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WalletCurrencyIncorrectException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "WalletCurrencyIncorrectException")
    public JAXBElement<WalletCurrencyIncorrectException> createWalletCurrencyIncorrectException(WalletCurrencyIncorrectException value) {
        return new JAXBElement<WalletCurrencyIncorrectException>(_WalletCurrencyIncorrectException_QNAME, WalletCurrencyIncorrectException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ValidateAccount }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "validateAccount")
    public JAXBElement<ValidateAccount> createValidateAccount(ValidateAccount value) {
        return new JAXBElement<ValidateAccount>(_ValidateAccount_QNAME, ValidateAccount.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ValidateAccountsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "validateAccountsResponse")
    public JAXBElement<ValidateAccountsResponse> createValidateAccountsResponse(ValidateAccountsResponse value) {
        return new JAXBElement<ValidateAccountsResponse>(_ValidateAccountsResponse_QNAME, ValidateAccountsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ValidateAccounts }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "validateAccounts")
    public JAXBElement<ValidateAccounts> createValidateAccounts(ValidateAccounts value) {
        return new JAXBElement<ValidateAccounts>(_ValidateAccounts_QNAME, ValidateAccounts.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SendMoneyToWexResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "sendMoneyToWexResponse")
    public JAXBElement<SendMoneyToWexResponse> createSendMoneyToWexResponse(SendMoneyToWexResponse value) {
        return new JAXBElement<SendMoneyToWexResponse>(_SendMoneyToWexResponse_QNAME, SendMoneyToWexResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RegistrationException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "RegistrationException")
    public JAXBElement<RegistrationException> createRegistrationException(RegistrationException value) {
        return new JAXBElement<RegistrationException>(_RegistrationException_QNAME, RegistrationException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Register }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "register")
    public JAXBElement<Register> createRegister(Register value) {
        return new JAXBElement<Register>(_Register_QNAME, Register.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ApiException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "ApiException")
    public JAXBElement<ApiException> createApiException(ApiException value) {
        return new JAXBElement<ApiException>(_ApiException_QNAME, ApiException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CodeIsNotValidException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "CodeIsNotValidException")
    public JAXBElement<CodeIsNotValidException> createCodeIsNotValidException(CodeIsNotValidException value) {
        return new JAXBElement<CodeIsNotValidException>(_CodeIsNotValidException_QNAME, CodeIsNotValidException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindTransaction }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "findTransaction")
    public JAXBElement<FindTransaction> createFindTransaction(FindTransaction value) {
        return new JAXBElement<FindTransaction>(_FindTransaction_QNAME, FindTransaction.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WrongIpException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "WrongIpException")
    public JAXBElement<WrongIpException> createWrongIpException(WrongIpException value) {
        return new JAXBElement<WrongIpException>(_WrongIpException_QNAME, WrongIpException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ValidationSendMoneyToEcoinEUResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "validationSendMoneyToEcoinEUResponse")
    public JAXBElement<ValidationSendMoneyToEcoinEUResponse> createValidationSendMoneyToEcoinEUResponse(ValidationSendMoneyToEcoinEUResponse value) {
        return new JAXBElement<ValidationSendMoneyToEcoinEUResponse>(_ValidationSendMoneyToEcoinEUResponse_QNAME, ValidationSendMoneyToEcoinEUResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ValidationSendMoneyToEmail }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "validationSendMoneyToEmail")
    public JAXBElement<ValidationSendMoneyToEmail> createValidationSendMoneyToEmail(ValidationSendMoneyToEmail value) {
        return new JAXBElement<ValidationSendMoneyToEmail>(_ValidationSendMoneyToEmail_QNAME, ValidationSendMoneyToEmail.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SendMoneyToBankCard }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "sendMoneyToBankCard")
    public JAXBElement<SendMoneyToBankCard> createSendMoneyToBankCard(SendMoneyToBankCard value) {
        return new JAXBElement<SendMoneyToBankCard>(_SendMoneyToBankCard_QNAME, SendMoneyToBankCard.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ValidationSendMoneyToEcoinEU }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "validationSendMoneyToEcoinEU")
    public JAXBElement<ValidationSendMoneyToEcoinEU> createValidationSendMoneyToEcoinEU(ValidationSendMoneyToEcoinEU value) {
        return new JAXBElement<ValidationSendMoneyToEcoinEU>(_ValidationSendMoneyToEcoinEU_QNAME, ValidationSendMoneyToEcoinEU.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransactionTemporaryNotAvailableException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "TransactionTemporaryNotAvailableException")
    public JAXBElement<TransactionTemporaryNotAvailableException> createTransactionTemporaryNotAvailableException(TransactionTemporaryNotAvailableException value) {
        return new JAXBElement<TransactionTemporaryNotAvailableException>(_TransactionTemporaryNotAvailableException_QNAME, TransactionTemporaryNotAvailableException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ExchangeCurrencyException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "ExchangeCurrencyException")
    public JAXBElement<ExchangeCurrencyException> createExchangeCurrencyException(ExchangeCurrencyException value) {
        return new JAXBElement<ExchangeCurrencyException>(_ExchangeCurrencyException_QNAME, ExchangeCurrencyException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CardDoesNotExistException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "CardDoesNotExistException")
    public JAXBElement<CardDoesNotExistException> createCardDoesNotExistException(CardDoesNotExistException value) {
        return new JAXBElement<CardDoesNotExistException>(_CardDoesNotExistException_QNAME, CardDoesNotExistException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WalletDoesNotExist }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "WalletDoesNotExist")
    public JAXBElement<WalletDoesNotExist> createWalletDoesNotExist(WalletDoesNotExist value) {
        return new JAXBElement<WalletDoesNotExist>(_WalletDoesNotExist_QNAME, WalletDoesNotExist.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NotAuthException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "NotAuthException")
    public JAXBElement<NotAuthException> createNotAuthException(NotAuthException value) {
        return new JAXBElement<NotAuthException>(_NotAuthException_QNAME, NotAuthException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WrongParamsException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "WrongParamsException")
    public JAXBElement<WrongParamsException> createWrongParamsException(WrongParamsException value) {
        return new JAXBElement<WrongParamsException>(_WrongParamsException_QNAME, WrongParamsException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ValidateWithdrawalThroughExternalPaymentSystemResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "validateWithdrawalThroughExternalPaymentSystemResponse")
    public JAXBElement<ValidateWithdrawalThroughExternalPaymentSystemResponse> createValidateWithdrawalThroughExternalPaymentSystemResponse(ValidateWithdrawalThroughExternalPaymentSystemResponse value) {
        return new JAXBElement<ValidateWithdrawalThroughExternalPaymentSystemResponse>(_ValidateWithdrawalThroughExternalPaymentSystemResponse_QNAME, ValidateWithdrawalThroughExternalPaymentSystemResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DuplicateOrderIdException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "DuplicateOrderIdException")
    public JAXBElement<DuplicateOrderIdException> createDuplicateOrderIdException(DuplicateOrderIdException value) {
        return new JAXBElement<DuplicateOrderIdException>(_DuplicateOrderIdException_QNAME, DuplicateOrderIdException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ValidationSendMoneyResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "validationSendMoneyResponse")
    public JAXBElement<ValidationSendMoneyResponse> createValidationSendMoneyResponse(ValidationSendMoneyResponse value) {
        return new JAXBElement<ValidationSendMoneyResponse>(_ValidationSendMoneyResponse_QNAME, ValidationSendMoneyResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MerchantDisabledException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "MerchantDisabledException")
    public JAXBElement<MerchantDisabledException> createMerchantDisabledException(MerchantDisabledException value) {
        return new JAXBElement<MerchantDisabledException>(_MerchantDisabledException_QNAME, MerchantDisabledException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CurrencyExchangeResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "currencyExchangeResponse")
    public JAXBElement<CurrencyExchangeResponse> createCurrencyExchangeResponse(CurrencyExchangeResponse value) {
        return new JAXBElement<CurrencyExchangeResponse>(_CurrencyExchangeResponse_QNAME, CurrencyExchangeResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ValidateCurrencyExchange }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "validateCurrencyExchange")
    public JAXBElement<ValidateCurrencyExchange> createValidateCurrencyExchange(ValidateCurrencyExchange value) {
        return new JAXBElement<ValidateCurrencyExchange>(_ValidateCurrencyExchange_QNAME, ValidateCurrencyExchange.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CardNumberIsNotValidException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "CardNumberIsNotValidException")
    public JAXBElement<CardNumberIsNotValidException> createCardNumberIsNotValidException(CardNumberIsNotValidException value) {
        return new JAXBElement<CardNumberIsNotValidException>(_CardNumberIsNotValidException_QNAME, CardNumberIsNotValidException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SendMoneyResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "sendMoneyResponse")
    public JAXBElement<SendMoneyResponse> createSendMoneyResponse(SendMoneyResponse value) {
        return new JAXBElement<SendMoneyResponse>(_SendMoneyResponse_QNAME, SendMoneyResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransactionIsNotAvailableException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "TransactionIsNotAvailableException")
    public JAXBElement<TransactionIsNotAvailableException> createTransactionIsNotAvailableException(TransactionIsNotAvailableException value) {
        return new JAXBElement<TransactionIsNotAvailableException>(_TransactionIsNotAvailableException_QNAME, TransactionIsNotAvailableException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NotAvailableDepositSystemException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "NotAvailableDepositSystemException")
    public JAXBElement<NotAvailableDepositSystemException> createNotAvailableDepositSystemException(NotAvailableDepositSystemException value) {
        return new JAXBElement<NotAvailableDepositSystemException>(_NotAvailableDepositSystemException_QNAME, NotAvailableDepositSystemException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmailTransferResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "emailTransferResponse")
    public JAXBElement<EmailTransferResponse> createEmailTransferResponse(EmailTransferResponse value) {
        return new JAXBElement<EmailTransferResponse>(_EmailTransferResponse_QNAME, EmailTransferResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ValidateAdvcashCardTransferResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "validateAdvcashCardTransferResponse")
    public JAXBElement<ValidateAdvcashCardTransferResponse> createValidateAdvcashCardTransferResponse(ValidateAdvcashCardTransferResponse value) {
        return new JAXBElement<ValidateAdvcashCardTransferResponse>(_ValidateAdvcashCardTransferResponse_QNAME, ValidateAdvcashCardTransferResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LimitPerDayException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "LimitPerDayException")
    public JAXBElement<LimitPerDayException> createLimitPerDayException(LimitPerDayException value) {
        return new JAXBElement<LimitPerDayException>(_LimitPerDayException_QNAME, LimitPerDayException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DatabaseException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "DatabaseException")
    public JAXBElement<DatabaseException> createDatabaseException(DatabaseException value) {
        return new JAXBElement<DatabaseException>(_DatabaseException_QNAME, DatabaseException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SendMoneyToEcoinEUResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "sendMoneyToEcoinEUResponse")
    public JAXBElement<SendMoneyToEcoinEUResponse> createSendMoneyToEcoinEUResponse(SendMoneyToEcoinEUResponse value) {
        return new JAXBElement<SendMoneyToEcoinEUResponse>(_SendMoneyToEcoinEUResponse_QNAME, SendMoneyToEcoinEUResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WithdrawalThroughExternalPaymentSystemResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "withdrawalThroughExternalPaymentSystemResponse")
    public JAXBElement<WithdrawalThroughExternalPaymentSystemResponse> createWithdrawalThroughExternalPaymentSystemResponse(WithdrawalThroughExternalPaymentSystemResponse value) {
        return new JAXBElement<WithdrawalThroughExternalPaymentSystemResponse>(_WithdrawalThroughExternalPaymentSystemResponse_QNAME, WithdrawalThroughExternalPaymentSystemResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ValidationSendMoneyToWex }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "validationSendMoneyToWex")
    public JAXBElement<ValidationSendMoneyToWex> createValidationSendMoneyToWex(ValidationSendMoneyToWex value) {
        return new JAXBElement<ValidationSendMoneyToWex>(_ValidationSendMoneyToWex_QNAME, ValidationSendMoneyToWex.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MakeCurrencyExchangeResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "makeCurrencyExchangeResponse")
    public JAXBElement<MakeCurrencyExchangeResponse> createMakeCurrencyExchangeResponse(MakeCurrencyExchangeResponse value) {
        return new JAXBElement<MakeCurrencyExchangeResponse>(_MakeCurrencyExchangeResponse_QNAME, MakeCurrencyExchangeResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ValidateAccountResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "validateAccountResponse")
    public JAXBElement<ValidateAccountResponse> createValidateAccountResponse(ValidateAccountResponse value) {
        return new JAXBElement<ValidateAccountResponse>(_ValidateAccountResponse_QNAME, ValidateAccountResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransferBankCardResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "transferBankCardResponse")
    public JAXBElement<TransferBankCardResponse> createTransferBankCardResponse(TransferBankCardResponse value) {
        return new JAXBElement<TransferBankCardResponse>(_TransferBankCardResponse_QNAME, TransferBankCardResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ValidationSendMoneyToBankCard }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "validationSendMoneyToBankCard")
    public JAXBElement<ValidationSendMoneyToBankCard> createValidationSendMoneyToBankCard(ValidationSendMoneyToBankCard value) {
        return new JAXBElement<ValidationSendMoneyToBankCard>(_ValidationSendMoneyToBankCard_QNAME, ValidationSendMoneyToBankCard.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SendMoneyToAdvcashCard }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "sendMoneyToAdvcashCard")
    public JAXBElement<SendMoneyToAdvcashCard> createSendMoneyToAdvcashCard(SendMoneyToAdvcashCard value) {
        return new JAXBElement<SendMoneyToAdvcashCard>(_SendMoneyToAdvcashCard_QNAME, SendMoneyToAdvcashCard.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransactionFailureException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "TransactionFailureException")
    public JAXBElement<TransactionFailureException> createTransactionFailureException(TransactionFailureException value) {
        return new JAXBElement<TransactionFailureException>(_TransactionFailureException_QNAME, TransactionFailureException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CurrencyExchange }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "currencyExchange")
    public JAXBElement<CurrencyExchange> createCurrencyExchange(CurrencyExchange value) {
        return new JAXBElement<CurrencyExchange>(_CurrencyExchange_QNAME, CurrencyExchange.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ValidationCurrencyExchangeResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "validationCurrencyExchangeResponse")
    public JAXBElement<ValidationCurrencyExchangeResponse> createValidationCurrencyExchangeResponse(ValidationCurrencyExchangeResponse value) {
        return new JAXBElement<ValidationCurrencyExchangeResponse>(_ValidationCurrencyExchangeResponse_QNAME, ValidationCurrencyExchangeResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransferAdvcashCard }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "transferAdvcashCard")
    public JAXBElement<TransferAdvcashCard> createTransferAdvcashCard(TransferAdvcashCard value) {
        return new JAXBElement<TransferAdvcashCard>(_TransferAdvcashCard_QNAME, TransferAdvcashCard.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SendMoneyToEmailResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "sendMoneyToEmailResponse")
    public JAXBElement<SendMoneyToEmailResponse> createSendMoneyToEmailResponse(SendMoneyToEmailResponse value) {
        return new JAXBElement<SendMoneyToEmailResponse>(_SendMoneyToEmailResponse_QNAME, SendMoneyToEmailResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WithdrawalThroughExternalPaymentSystem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "withdrawalThroughExternalPaymentSystem")
    public JAXBElement<WithdrawalThroughExternalPaymentSystem> createWithdrawalThroughExternalPaymentSystem(WithdrawalThroughExternalPaymentSystem value) {
        return new JAXBElement<WithdrawalThroughExternalPaymentSystem>(_WithdrawalThroughExternalPaymentSystem_QNAME, WithdrawalThroughExternalPaymentSystem.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ValidateAdvcashCardTransfer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "validateAdvcashCardTransfer")
    public JAXBElement<ValidateAdvcashCardTransfer> createValidateAdvcashCardTransfer(ValidateAdvcashCardTransfer value) {
        return new JAXBElement<ValidateAdvcashCardTransfer>(_ValidateAdvcashCardTransfer_QNAME, ValidateAdvcashCardTransfer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindTransactionResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "findTransactionResponse")
    public JAXBElement<FindTransactionResponse> createFindTransactionResponse(FindTransactionResponse value) {
        return new JAXBElement<FindTransactionResponse>(_FindTransactionResponse_QNAME, FindTransactionResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ValidationSendMoneyToEmailResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "validationSendMoneyToEmailResponse")
    public JAXBElement<ValidationSendMoneyToEmailResponse> createValidationSendMoneyToEmailResponse(ValidationSendMoneyToEmailResponse value) {
        return new JAXBElement<ValidationSendMoneyToEmailResponse>(_ValidationSendMoneyToEmailResponse_QNAME, ValidationSendMoneyToEmailResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ValidateTransfer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "validateTransfer")
    public JAXBElement<ValidateTransfer> createValidateTransfer(ValidateTransfer value) {
        return new JAXBElement<ValidateTransfer>(_ValidateTransfer_QNAME, ValidateTransfer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MakeTransferResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "makeTransferResponse")
    public JAXBElement<MakeTransferResponse> createMakeTransferResponse(MakeTransferResponse value) {
        return new JAXBElement<MakeTransferResponse>(_MakeTransferResponse_QNAME, MakeTransferResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SendMoneyToWex }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "sendMoneyToWex")
    public JAXBElement<SendMoneyToWex> createSendMoneyToWex(SendMoneyToWex value) {
        return new JAXBElement<SendMoneyToWex>(_SendMoneyToWex_QNAME, SendMoneyToWex.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InternalException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "InternalException")
    public JAXBElement<InternalException> createInternalException(InternalException value) {
        return new JAXBElement<InternalException>(_InternalException_QNAME, InternalException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ValidateTransferResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "validateTransferResponse")
    public JAXBElement<ValidateTransferResponse> createValidateTransferResponse(ValidateTransferResponse value) {
        return new JAXBElement<ValidateTransferResponse>(_ValidateTransferResponse_QNAME, ValidateTransferResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WrongEmailException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "WrongEmailException")
    public JAXBElement<WrongEmailException> createWrongEmailException(WrongEmailException value) {
        return new JAXBElement<WrongEmailException>(_WrongEmailException_QNAME, WrongEmailException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SendMoneyToExmo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "sendMoneyToExmo")
    public JAXBElement<SendMoneyToExmo> createSendMoneyToExmo(SendMoneyToExmo value) {
        return new JAXBElement<SendMoneyToExmo>(_SendMoneyToExmo_QNAME, SendMoneyToExmo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetBalancesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "getBalancesResponse")
    public JAXBElement<GetBalancesResponse> createGetBalancesResponse(GetBalancesResponse value) {
        return new JAXBElement<GetBalancesResponse>(_GetBalancesResponse_QNAME, GetBalancesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MakeCurrencyExchange }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "makeCurrencyExchange")
    public JAXBElement<MakeCurrencyExchange> createMakeCurrencyExchange(MakeCurrencyExchange value) {
        return new JAXBElement<MakeCurrencyExchange>(_MakeCurrencyExchange_QNAME, MakeCurrencyExchange.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RegisterResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "registerResponse")
    public JAXBElement<RegisterResponse> createRegisterResponse(RegisterResponse value) {
        return new JAXBElement<RegisterResponse>(_RegisterResponse_QNAME, RegisterResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateBitcoinInvoiceResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "createBitcoinInvoiceResponse")
    public JAXBElement<CreateBitcoinInvoiceResponse> createCreateBitcoinInvoiceResponse(CreateBitcoinInvoiceResponse value) {
        return new JAXBElement<CreateBitcoinInvoiceResponse>(_CreateBitcoinInvoiceResponse_QNAME, CreateBitcoinInvoiceResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransferBankCard }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "transferBankCard")
    public JAXBElement<TransferBankCard> createTransferBankCard(TransferBankCard value) {
        return new JAXBElement<TransferBankCard>(_TransferBankCard_QNAME, TransferBankCard.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LimitPerCardPerDayException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "LimitPerCardPerDayException")
    public JAXBElement<LimitPerCardPerDayException> createLimitPerCardPerDayException(LimitPerCardPerDayException value) {
        return new JAXBElement<LimitPerCardPerDayException>(_LimitPerCardPerDayException_QNAME, LimitPerCardPerDayException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ValidationSendMoneyToExmoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "validationSendMoneyToExmoResponse")
    public JAXBElement<ValidationSendMoneyToExmoResponse> createValidationSendMoneyToExmoResponse(ValidationSendMoneyToExmoResponse value) {
        return new JAXBElement<ValidationSendMoneyToExmoResponse>(_ValidationSendMoneyToExmoResponse_QNAME, ValidationSendMoneyToExmoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MakeTransfer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "makeTransfer")
    public JAXBElement<MakeTransfer> createMakeTransfer(MakeTransfer value) {
        return new JAXBElement<MakeTransfer>(_MakeTransfer_QNAME, MakeTransfer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link HistoryResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "historyResponse")
    public JAXBElement<HistoryResponse> createHistoryResponse(HistoryResponse value) {
        return new JAXBElement<HistoryResponse>(_HistoryResponse_QNAME, HistoryResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ValidationSendMoneyToEcurrencyResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "validationSendMoneyToEcurrencyResponse")
    public JAXBElement<ValidationSendMoneyToEcurrencyResponse> createValidationSendMoneyToEcurrencyResponse(ValidationSendMoneyToEcurrencyResponse value) {
        return new JAXBElement<ValidationSendMoneyToEcurrencyResponse>(_ValidationSendMoneyToEcurrencyResponse_QNAME, ValidationSendMoneyToEcurrencyResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ValidateWithdrawalThroughExternalPaymentSystem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "validateWithdrawalThroughExternalPaymentSystem")
    public JAXBElement<ValidateWithdrawalThroughExternalPaymentSystem> createValidateWithdrawalThroughExternalPaymentSystem(ValidateWithdrawalThroughExternalPaymentSystem value) {
        return new JAXBElement<ValidateWithdrawalThroughExternalPaymentSystem>(_ValidateWithdrawalThroughExternalPaymentSystem_QNAME, ValidateWithdrawalThroughExternalPaymentSystem.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ValidateEmailTransfer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "validateEmailTransfer")
    public JAXBElement<ValidateEmailTransfer> createValidateEmailTransfer(ValidateEmailTransfer value) {
        return new JAXBElement<ValidateEmailTransfer>(_ValidateEmailTransfer_QNAME, ValidateEmailTransfer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LimitsException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "LimitsException")
    public JAXBElement<LimitsException> createLimitsException(LimitsException value) {
        return new JAXBElement<LimitsException>(_LimitsException_QNAME, LimitsException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CheckCurrencyExchange }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "checkCurrencyExchange")
    public JAXBElement<CheckCurrencyExchange> createCheckCurrencyExchange(CheckCurrencyExchange value) {
        return new JAXBElement<CheckCurrencyExchange>(_CheckCurrencyExchange_QNAME, CheckCurrencyExchange.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateBitcoinInvoice }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "createBitcoinInvoice")
    public JAXBElement<CreateBitcoinInvoice> createCreateBitcoinInvoice(CreateBitcoinInvoice value) {
        return new JAXBElement<CreateBitcoinInvoice>(_CreateBitcoinInvoice_QNAME, CreateBitcoinInvoice.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NotEnoughMoneyException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "NotEnoughMoneyException")
    public JAXBElement<NotEnoughMoneyException> createNotEnoughMoneyException(NotEnoughMoneyException value) {
        return new JAXBElement<NotEnoughMoneyException>(_NotEnoughMoneyException_QNAME, NotEnoughMoneyException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ValidateBankCardTransferResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "validateBankCardTransferResponse")
    public JAXBElement<ValidateBankCardTransferResponse> createValidateBankCardTransferResponse(ValidateBankCardTransferResponse value) {
        return new JAXBElement<ValidateBankCardTransferResponse>(_ValidateBankCardTransferResponse_QNAME, ValidateBankCardTransferResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LimitPerTransactionException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "LimitPerTransactionException")
    public JAXBElement<LimitPerTransactionException> createLimitPerTransactionException(LimitPerTransactionException value) {
        return new JAXBElement<LimitPerTransactionException>(_LimitPerTransactionException_QNAME, LimitPerTransactionException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ValidationCurrencyExchange }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "validationCurrencyExchange")
    public JAXBElement<ValidationCurrencyExchange> createValidationCurrencyExchange(ValidationCurrencyExchange value) {
        return new JAXBElement<ValidationCurrencyExchange>(_ValidationCurrencyExchange_QNAME, ValidationCurrencyExchange.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link History }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "history")
    public JAXBElement<History> createHistory(History value) {
        return new JAXBElement<History>(_History_QNAME, History.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UserBlockedException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "UserBlockedException")
    public JAXBElement<UserBlockedException> createUserBlockedException(UserBlockedException value) {
        return new JAXBElement<UserBlockedException>(_UserBlockedException_QNAME, UserBlockedException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ValidationSendMoneyToAdvcashCardResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "validationSendMoneyToAdvcashCardResponse")
    public JAXBElement<ValidationSendMoneyToAdvcashCardResponse> createValidationSendMoneyToAdvcashCardResponse(ValidationSendMoneyToAdvcashCardResponse value) {
        return new JAXBElement<ValidationSendMoneyToAdvcashCardResponse>(_ValidationSendMoneyToAdvcashCardResponse_QNAME, ValidationSendMoneyToAdvcashCardResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link JAXBException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "JAXBException")
    public JAXBElement<JAXBException> createJAXBException(JAXBException value) {
        return new JAXBElement<JAXBException>(_JAXBException_QNAME, JAXBException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CallRestrictionException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "CallRestrictionException")
    public JAXBElement<CallRestrictionException> createCallRestrictionException(CallRestrictionException value) {
        return new JAXBElement<CallRestrictionException>(_CallRestrictionException_QNAME, CallRestrictionException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SendMoneyToBankCardResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "sendMoneyToBankCardResponse")
    public JAXBElement<SendMoneyToBankCardResponse> createSendMoneyToBankCardResponse(SendMoneyToBankCardResponse value) {
        return new JAXBElement<SendMoneyToBankCardResponse>(_SendMoneyToBankCardResponse_QNAME, SendMoneyToBankCardResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NotSupportedBankBinException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "NotSupportedBankBinException")
    public JAXBElement<NotSupportedBankBinException> createNotSupportedBankBinException(NotSupportedBankBinException value) {
        return new JAXBElement<NotSupportedBankBinException>(_NotSupportedBankBinException_QNAME, NotSupportedBankBinException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SendMoneyToExmoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "sendMoneyToExmoResponse")
    public JAXBElement<SendMoneyToExmoResponse> createSendMoneyToExmoResponse(SendMoneyToExmoResponse value) {
        return new JAXBElement<SendMoneyToExmoResponse>(_SendMoneyToExmoResponse_QNAME, SendMoneyToExmoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LifetimeLimitException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "LifetimeLimitException")
    public JAXBElement<LifetimeLimitException> createLifetimeLimitException(LifetimeLimitException value) {
        return new JAXBElement<LifetimeLimitException>(_LifetimeLimitException_QNAME, LifetimeLimitException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CheckCurrencyExchangeResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "checkCurrencyExchangeResponse")
    public JAXBElement<CheckCurrencyExchangeResponse> createCheckCurrencyExchangeResponse(CheckCurrencyExchangeResponse value) {
        return new JAXBElement<CheckCurrencyExchangeResponse>(_CheckCurrencyExchangeResponse_QNAME, CheckCurrencyExchangeResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ValidateCurrencyExchangeResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "validateCurrencyExchangeResponse")
    public JAXBElement<ValidateCurrencyExchangeResponse> createValidateCurrencyExchangeResponse(ValidateCurrencyExchangeResponse value) {
        return new JAXBElement<ValidateCurrencyExchangeResponse>(_ValidateCurrencyExchangeResponse_QNAME, ValidateCurrencyExchangeResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SendMoneyToEcurrencyResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "sendMoneyToEcurrencyResponse")
    public JAXBElement<SendMoneyToEcurrencyResponse> createSendMoneyToEcurrencyResponse(SendMoneyToEcurrencyResponse value) {
        return new JAXBElement<SendMoneyToEcurrencyResponse>(_SendMoneyToEcurrencyResponse_QNAME, SendMoneyToEcurrencyResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SendMoneyToEmail }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "sendMoneyToEmail")
    public JAXBElement<SendMoneyToEmail> createSendMoneyToEmail(SendMoneyToEmail value) {
        return new JAXBElement<SendMoneyToEmail>(_SendMoneyToEmail_QNAME, SendMoneyToEmail.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SendMoney }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "sendMoney")
    public JAXBElement<SendMoney> createSendMoney(SendMoney value) {
        return new JAXBElement<SendMoney>(_SendMoney_QNAME, SendMoney.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ValidationSendMoneyToEcurrency }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "validationSendMoneyToEcurrency")
    public JAXBElement<ValidationSendMoneyToEcurrency> createValidationSendMoneyToEcurrency(ValidationSendMoneyToEcurrency value) {
        return new JAXBElement<ValidationSendMoneyToEcurrency>(_ValidationSendMoneyToEcurrency_QNAME, ValidationSendMoneyToEcurrency.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CardIsNotActiveException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "CardIsNotActiveException")
    public JAXBElement<CardIsNotActiveException> createCardIsNotActiveException(CardIsNotActiveException value) {
        return new JAXBElement<CardIsNotActiveException>(_CardIsNotActiveException_QNAME, CardIsNotActiveException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SendMoneyToAdvcashCardResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "sendMoneyToAdvcashCardResponse")
    public JAXBElement<SendMoneyToAdvcashCardResponse> createSendMoneyToAdvcashCardResponse(SendMoneyToAdvcashCardResponse value) {
        return new JAXBElement<SendMoneyToAdvcashCardResponse>(_SendMoneyToAdvcashCardResponse_QNAME, SendMoneyToAdvcashCardResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdditionalDataRequiredException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "AdditionalDataRequiredException")
    public JAXBElement<AdditionalDataRequiredException> createAdditionalDataRequiredException(AdditionalDataRequiredException value) {
        return new JAXBElement<AdditionalDataRequiredException>(_AdditionalDataRequiredException_QNAME, AdditionalDataRequiredException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SendMoneyToEcurrency }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "sendMoneyToEcurrency")
    public JAXBElement<SendMoneyToEcurrency> createSendMoneyToEcurrency(SendMoneyToEcurrency value) {
        return new JAXBElement<SendMoneyToEcurrency>(_SendMoneyToEcurrency_QNAME, SendMoneyToEcurrency.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UserDoesNotExistException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "UserDoesNotExistException")
    public JAXBElement<UserDoesNotExistException> createUserDoesNotExistException(UserDoesNotExistException value) {
        return new JAXBElement<UserDoesNotExistException>(_UserDoesNotExistException_QNAME, UserDoesNotExistException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ValidateBankCardTransfer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "validateBankCardTransfer")
    public JAXBElement<ValidateBankCardTransfer> createValidateBankCardTransfer(ValidateBankCardTransfer value) {
        return new JAXBElement<ValidateBankCardTransfer>(_ValidateBankCardTransfer_QNAME, ValidateBankCardTransfer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ValidateEmailTransferResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "validateEmailTransferResponse")
    public JAXBElement<ValidateEmailTransferResponse> createValidateEmailTransferResponse(ValidateEmailTransferResponse value) {
        return new JAXBElement<ValidateEmailTransferResponse>(_ValidateEmailTransferResponse_QNAME, ValidateEmailTransferResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmailTransfer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "emailTransfer")
    public JAXBElement<EmailTransfer> createEmailTransfer(EmailTransfer value) {
        return new JAXBElement<EmailTransfer>(_EmailTransfer_QNAME, EmailTransfer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ValidationSendMoneyToWexResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "validationSendMoneyToWexResponse")
    public JAXBElement<ValidationSendMoneyToWexResponse> createValidationSendMoneyToWexResponse(ValidationSendMoneyToWexResponse value) {
        return new JAXBElement<ValidationSendMoneyToWexResponse>(_ValidationSendMoneyToWexResponse_QNAME, ValidationSendMoneyToWexResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ValidationSendMoneyToExmo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "validationSendMoneyToExmo")
    public JAXBElement<ValidationSendMoneyToExmo> createValidationSendMoneyToExmo(ValidationSendMoneyToExmo value) {
        return new JAXBElement<ValidationSendMoneyToExmo>(_ValidationSendMoneyToExmo_QNAME, ValidationSendMoneyToExmo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AccessDeniedException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "AccessDeniedException")
    public JAXBElement<AccessDeniedException> createAccessDeniedException(AccessDeniedException value) {
        return new JAXBElement<AccessDeniedException>(_AccessDeniedException_QNAME, AccessDeniedException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ValidationSendMoneyToBankCardResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "validationSendMoneyToBankCardResponse")
    public JAXBElement<ValidationSendMoneyToBankCardResponse> createValidationSendMoneyToBankCardResponse(ValidationSendMoneyToBankCardResponse value) {
        return new JAXBElement<ValidationSendMoneyToBankCardResponse>(_ValidationSendMoneyToBankCardResponse_QNAME, ValidationSendMoneyToBankCardResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmailAlreadyExistException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "EmailAlreadyExistException")
    public JAXBElement<EmailAlreadyExistException> createEmailAlreadyExistException(EmailAlreadyExistException value) {
        return new JAXBElement<EmailAlreadyExistException>(_EmailAlreadyExistException_QNAME, EmailAlreadyExistException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransferAdvcashCardResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "transferAdvcashCardResponse")
    public JAXBElement<TransferAdvcashCardResponse> createTransferAdvcashCardResponse(TransferAdvcashCardResponse value) {
        return new JAXBElement<TransferAdvcashCardResponse>(_TransferAdvcashCardResponse_QNAME, TransferAdvcashCardResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NotSupportedCountryException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "NotSupportedCountryException")
    public JAXBElement<NotSupportedCountryException> createNotSupportedCountryException(NotSupportedCountryException value) {
        return new JAXBElement<NotSupportedCountryException>(_NotSupportedCountryException_QNAME, NotSupportedCountryException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NotEnoughMoneyApiException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "NotEnoughMoneyApiException")
    public JAXBElement<NotEnoughMoneyApiException> createNotEnoughMoneyApiException(NotEnoughMoneyApiException value) {
        return new JAXBElement<NotEnoughMoneyApiException>(_NotEnoughMoneyApiException_QNAME, NotEnoughMoneyApiException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ValidationSendMoney }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "validationSendMoney")
    public JAXBElement<ValidationSendMoney> createValidationSendMoney(ValidationSendMoney value) {
        return new JAXBElement<ValidationSendMoney>(_ValidationSendMoney_QNAME, ValidationSendMoney.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetBalances }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsm.advcash/", name = "getBalances")
    public JAXBElement<GetBalances> createGetBalances(GetBalances value) {
        return new JAXBElement<GetBalances>(_GetBalances_QNAME, GetBalances.class, null, value);
    }

}
