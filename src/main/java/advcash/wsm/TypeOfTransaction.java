
package advcash.wsm;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for typeOfTransaction.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="typeOfTransaction">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="TRANSFER_INNER_SYSTEM"/>
 *     &lt;enumeration value="TRANSFER_INNER_ACCOUNT"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "typeOfTransaction")
@XmlEnum
public enum TypeOfTransaction {

    TRANSFER_INNER_SYSTEM,
    TRANSFER_INNER_ACCOUNT;

    public String value() {
        return name();
    }

    public static TypeOfTransaction fromValue(String v) {
        return valueOf(v);
    }

}
