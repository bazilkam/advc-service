
package advcash.wsm;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for externalSystemWithdrawalType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="externalSystemWithdrawalType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="OK_PAY_ELECTRONIC_MONEY"/>
 *     &lt;enumeration value="PERFECT_MONEY_ELECTRONIC_MONEY"/>
 *     &lt;enumeration value="EGO_PAY_ELECTRONIC_MONEY"/>
 *     &lt;enumeration value="PAXUM_ELECTRONIC_MONEY"/>
 *     &lt;enumeration value="YANDEX_ELECTRONIC_MONEY"/>
 *     &lt;enumeration value="PAYEER_ELECTRONIC_MONEY"/>
 *     &lt;enumeration value="EXMO_BITCOIN_ELECTRONIC_MONEY"/>
 *     &lt;enumeration value="COINBASE_ELECTRONIC_MONEY"/>
 *     &lt;enumeration value="KITPS_PAYMENT_SYSTEM"/>
 *     &lt;enumeration value="TEST_PAYMENT_SYSTEM"/>
 *     &lt;enumeration value="BTC_E_BITCOIN_ELECTRONIC_MONEY"/>
 *     &lt;enumeration value="NETEX_WEB_MONEY_ELECTRONIC_MONEY"/>
 *     &lt;enumeration value="BTC_E_ELECTRONIC_MONEY"/>
 *     &lt;enumeration value="EXMO_ELECTRONIC_MONEY"/>
 *     &lt;enumeration value="ECOIN_ELECTRONIC_MONEY"/>
 *     &lt;enumeration value="CAPITALIST_ELECTRONIC_MONEY"/>
 *     &lt;enumeration value="MONETARU_RUSSIAN_BANK_CARD"/>
 *     &lt;enumeration value="MONETARU_QIWI"/>
 *     &lt;enumeration value="INTERKASSA_UKRAINIAN_BANK_CARD"/>
 *     &lt;enumeration value="BITOK_ELECTRONIC_MONEY"/>
 *     &lt;enumeration value="PAYZA_ELECTRONIC_MONEY"/>
 *     &lt;enumeration value="EPESE_ELECTRONIC_MONEY"/>
 *     &lt;enumeration value="PAYBOX_KZ_KAZAKHSTANI_BANK_CARD"/>
 *     &lt;enumeration value="QIWI_ELECTRONIC_MONEY"/>
 *     &lt;enumeration value="CREDIT_PILOT_RU_CARD"/>
 *     &lt;enumeration value="CREDIT_PILOT_RU_BANK"/>
 *     &lt;enumeration value="CREDIT_PILOT_WALLET_ONE"/>
 *     &lt;enumeration value="CREDIT_PILOT_SERVICES_OTHER"/>
 *     &lt;enumeration value="CREDIT_PILOT_SERVICES_BANK_PAYMENTS"/>
 *     &lt;enumeration value="CREDIT_PILOT_SERVICES_MOBILE_SERVICES"/>
 *     &lt;enumeration value="CREDIT_PILOT_SERVICES_TAXI"/>
 *     &lt;enumeration value="CREDIT_PILOT_SERVICES_GAMES"/>
 *     &lt;enumeration value="CREDIT_PILOT_SERVICES_INTERNET_TV_TELECOM"/>
 *     &lt;enumeration value="CREDIT_PILOT_SERVICES_E_COMMERCE"/>
 *     &lt;enumeration value="CREDIT_PILOT_SERVICES_UTILITY_BILLS"/>
 *     &lt;enumeration value="CREDIT_PILOT_SERVICES_MICROFINANCE_ORGANIZATIONS"/>
 *     &lt;enumeration value="ADVCASH_VOUCHER"/>
 *     &lt;enumeration value="WINPAY_BANK_CARD"/>
 *     &lt;enumeration value="BIT_ONE_LOCAL_BANK_TRANSFER"/>
 *     &lt;enumeration value="BTC_E_ETHEREUM_ELECTRONIC_MONEY"/>
 *     &lt;enumeration value="EPAY_ELECTRONIC_MONEY"/>
 *     &lt;enumeration value="YANDEX_CASSA_ELECTRONIC_MONEY"/>
 *     &lt;enumeration value="ECOIN_VOUCHER_ELECTRONIC_MONEY"/>
 *     &lt;enumeration value="PAYBOX_KZ_CASH_BY_CODE"/>
 *     &lt;enumeration value="WEX_ELECTRONIC_MONEY"/>
 *     &lt;enumeration value="WEX_BITCOIN_ELECTRONIC_MONEY"/>
 *     &lt;enumeration value="WEX_ETHEREUM_ELECTRONIC_MONEY"/>
 *     &lt;enumeration value="WEX_BITCOIN_CASH_ELECTRONIC_MONEY"/>
 *     &lt;enumeration value="WINPAY_MOBILE"/>
 *     &lt;enumeration value="WINPAY_QIWI"/>
 *     &lt;enumeration value="WINPAY_YANDEX"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "externalSystemWithdrawalType")
@XmlEnum
public enum ExternalSystemWithdrawalType {

    OK_PAY_ELECTRONIC_MONEY,
    PERFECT_MONEY_ELECTRONIC_MONEY,
    EGO_PAY_ELECTRONIC_MONEY,
    PAXUM_ELECTRONIC_MONEY,
    YANDEX_ELECTRONIC_MONEY,
    PAYEER_ELECTRONIC_MONEY,
    EXMO_BITCOIN_ELECTRONIC_MONEY,
    COINBASE_ELECTRONIC_MONEY,
    KITPS_PAYMENT_SYSTEM,
    TEST_PAYMENT_SYSTEM,
    BTC_E_BITCOIN_ELECTRONIC_MONEY,
    NETEX_WEB_MONEY_ELECTRONIC_MONEY,
    BTC_E_ELECTRONIC_MONEY,
    EXMO_ELECTRONIC_MONEY,
    ECOIN_ELECTRONIC_MONEY,
    CAPITALIST_ELECTRONIC_MONEY,
    MONETARU_RUSSIAN_BANK_CARD,
    MONETARU_QIWI,
    INTERKASSA_UKRAINIAN_BANK_CARD,
    BITOK_ELECTRONIC_MONEY,
    PAYZA_ELECTRONIC_MONEY,
    EPESE_ELECTRONIC_MONEY,
    PAYBOX_KZ_KAZAKHSTANI_BANK_CARD,
    QIWI_ELECTRONIC_MONEY,
    CREDIT_PILOT_RU_CARD,
    CREDIT_PILOT_RU_BANK,
    CREDIT_PILOT_WALLET_ONE,
    CREDIT_PILOT_SERVICES_OTHER,
    CREDIT_PILOT_SERVICES_BANK_PAYMENTS,
    CREDIT_PILOT_SERVICES_MOBILE_SERVICES,
    CREDIT_PILOT_SERVICES_TAXI,
    CREDIT_PILOT_SERVICES_GAMES,
    CREDIT_PILOT_SERVICES_INTERNET_TV_TELECOM,
    CREDIT_PILOT_SERVICES_E_COMMERCE,
    CREDIT_PILOT_SERVICES_UTILITY_BILLS,
    CREDIT_PILOT_SERVICES_MICROFINANCE_ORGANIZATIONS,
    ADVCASH_VOUCHER,
    WINPAY_BANK_CARD,
    BIT_ONE_LOCAL_BANK_TRANSFER,
    BTC_E_ETHEREUM_ELECTRONIC_MONEY,
    EPAY_ELECTRONIC_MONEY,
    YANDEX_CASSA_ELECTRONIC_MONEY,
    ECOIN_VOUCHER_ELECTRONIC_MONEY,
    PAYBOX_KZ_CASH_BY_CODE,
    WEX_ELECTRONIC_MONEY,
    WEX_BITCOIN_ELECTRONIC_MONEY,
    WEX_ETHEREUM_ELECTRONIC_MONEY,
    WEX_BITCOIN_CASH_ELECTRONIC_MONEY,
    WINPAY_MOBILE,
    WINPAY_QIWI,
    WINPAY_YANDEX;

    public String value() {
        return name();
    }

    public static ExternalSystemWithdrawalType fromValue(String v) {
        return valueOf(v);
    }

}
