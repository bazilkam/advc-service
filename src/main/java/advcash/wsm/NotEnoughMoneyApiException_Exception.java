
package advcash.wsm;

import javax.xml.ws.WebFault;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.9-b130926.1035
 * Generated source version: 2.2
 * 
 */
@WebFault(name = "NotEnoughMoneyApiException", targetNamespace = "http://wsm.advcash/")
public class NotEnoughMoneyApiException_Exception
    extends Exception
{

    /**
     * Java type that goes as soapenv:Fault detail element.
     * 
     */
    private NotEnoughMoneyApiException faultInfo;

    /**
     * 
     * @param faultInfo
     * @param message
     */
    public NotEnoughMoneyApiException_Exception(String message, NotEnoughMoneyApiException faultInfo) {
        super(message);
        this.faultInfo = faultInfo;
    }

    /**
     * 
     * @param faultInfo
     * @param cause
     * @param message
     */
    public NotEnoughMoneyApiException_Exception(String message, NotEnoughMoneyApiException faultInfo, Throwable cause) {
        super(message, cause);
        this.faultInfo = faultInfo;
    }

    /**
     * 
     * @return
     *     returns fault bean: advcash.wsm.NotEnoughMoneyApiException
     */
    public NotEnoughMoneyApiException getFaultInfo() {
        return faultInfo;
    }

}
