
package advcash.wsm;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for transactionName.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="transactionName">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="ALL"/>
 *     &lt;enumeration value="CHECK_DEPOSIT"/>
 *     &lt;enumeration value="WIRE_TRANSFER_DEPOSIT"/>
 *     &lt;enumeration value="WIRE_TRANSFER_WITHDRAW"/>
 *     &lt;enumeration value="CHECK_WITHDRAW"/>
 *     &lt;enumeration value="INNER_ACCOUNT"/>
 *     &lt;enumeration value="INNER_SYSTEM"/>
 *     &lt;enumeration value="CURRENCY_EXCHANGE"/>
 *     &lt;enumeration value="MONEY_REQUEST"/>
 *     &lt;enumeration value="REFERRAL_PAYMENT"/>
 *     &lt;enumeration value="SYSTEM_PAYMENT"/>
 *     &lt;enumeration value="REPAYMENT"/>
 *     &lt;enumeration value="SCI_PAYMENT_OTHER_SYSTEM"/>
 *     &lt;enumeration value="BANK_CARD_TRANSFER"/>
 *     &lt;enumeration value="ADVCASH_CARD_TRANSFER"/>
 *     &lt;enumeration value="WIRE_TRANSFER_DEPOSIT_3RD_PARTY"/>
 *     &lt;enumeration value="EXTERNAL_SYSTEM_DEPOSIT"/>
 *     &lt;enumeration value="EXTERNAL_SYSTEM_WITHDRAWAL"/>
 *     &lt;enumeration value="BALANCE_ADJUSTMENT_DEPOSIT"/>
 *     &lt;enumeration value="BALANCE_ADJUSTMENT_WITHDRAWAL"/>
 *     &lt;enumeration value="SEPA_TRANSFER_DEPOSIT"/>
 *     &lt;enumeration value="SEPA_TRANSFER_DEPOSIT_3RD_PARTY"/>
 *     &lt;enumeration value="BONUS_PAYMENT"/>
 *     &lt;enumeration value="ADVCASH_CARD_UNLOAD"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "transactionName")
@XmlEnum
public enum TransactionName {

    ALL("ALL"),
    CHECK_DEPOSIT("CHECK_DEPOSIT"),
    WIRE_TRANSFER_DEPOSIT("WIRE_TRANSFER_DEPOSIT"),
    WIRE_TRANSFER_WITHDRAW("WIRE_TRANSFER_WITHDRAW"),
    CHECK_WITHDRAW("CHECK_WITHDRAW"),
    INNER_ACCOUNT("INNER_ACCOUNT"),
    INNER_SYSTEM("INNER_SYSTEM"),
    CURRENCY_EXCHANGE("CURRENCY_EXCHANGE"),
    MONEY_REQUEST("MONEY_REQUEST"),
    REFERRAL_PAYMENT("REFERRAL_PAYMENT"),
    SYSTEM_PAYMENT("SYSTEM_PAYMENT"),
    REPAYMENT("REPAYMENT"),
    SCI_PAYMENT_OTHER_SYSTEM("SCI_PAYMENT_OTHER_SYSTEM"),
    BANK_CARD_TRANSFER("BANK_CARD_TRANSFER"),
    ADVCASH_CARD_TRANSFER("ADVCASH_CARD_TRANSFER"),
    @XmlEnumValue("WIRE_TRANSFER_DEPOSIT_3RD_PARTY")
    WIRE_TRANSFER_DEPOSIT_3_RD_PARTY("WIRE_TRANSFER_DEPOSIT_3RD_PARTY"),
    EXTERNAL_SYSTEM_DEPOSIT("EXTERNAL_SYSTEM_DEPOSIT"),
    EXTERNAL_SYSTEM_WITHDRAWAL("EXTERNAL_SYSTEM_WITHDRAWAL"),
    BALANCE_ADJUSTMENT_DEPOSIT("BALANCE_ADJUSTMENT_DEPOSIT"),
    BALANCE_ADJUSTMENT_WITHDRAWAL("BALANCE_ADJUSTMENT_WITHDRAWAL"),
    SEPA_TRANSFER_DEPOSIT("SEPA_TRANSFER_DEPOSIT"),
    @XmlEnumValue("SEPA_TRANSFER_DEPOSIT_3RD_PARTY")
    SEPA_TRANSFER_DEPOSIT_3_RD_PARTY("SEPA_TRANSFER_DEPOSIT_3RD_PARTY"),
    BONUS_PAYMENT("BONUS_PAYMENT"),
    ADVCASH_CARD_UNLOAD("ADVCASH_CARD_UNLOAD");
    private final String value;

    TransactionName(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static TransactionName fromValue(String v) {
        for (TransactionName c: TransactionName.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
