
package advcash.wsm;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for exceptionType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="exceptionType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="AlreadyExistException"/>
 *     &lt;enumeration value="AttemptsHaveEndedException"/>
 *     &lt;enumeration value="BadParametersException"/>
 *     &lt;enumeration value="BadFormatAmountException"/>
 *     &lt;enumeration value="CodeHasExpired"/>
 *     &lt;enumeration value="CodeIsNotValidException"/>
 *     &lt;enumeration value="CryptoException"/>
 *     &lt;enumeration value="DatabaseException"/>
 *     &lt;enumeration value="EmailAlreadyExistException"/>
 *     &lt;enumeration value="ExchangeCurrencyException"/>
 *     &lt;enumeration value="FileFormatException"/>
 *     &lt;enumeration value="FileSystemException"/>
 *     &lt;enumeration value="GroupDoesNotEmptyException"/>
 *     &lt;enumeration value="IpAddressIsWrongException"/>
 *     &lt;enumeration value="LimitPerMonthException"/>
 *     &lt;enumeration value="LimitPerDayException"/>
 *     &lt;enumeration value="LimitPerTransactionException"/>
 *     &lt;enumeration value="LoginAlreadyExistException"/>
 *     &lt;enumeration value="NotEnoughMoneyException"/>
 *     &lt;enumeration value="PasswordExpiredException"/>
 *     &lt;enumeration value="PasswordIncorrectException"/>
 *     &lt;enumeration value="PasswordIsNotNewException"/>
 *     &lt;enumeration value="PINCodeIncorrectException"/>
 *     &lt;enumeration value="PasswordRecoveryException"/>
 *     &lt;enumeration value="PhoneNumberAlreadyExistException"/>
 *     &lt;enumeration value="PhoneNumberNotValidException"/>
 *     &lt;enumeration value="RecipientDoesNotExistException"/>
 *     &lt;enumeration value="RegistrationException"/>
 *     &lt;enumeration value="SecretAnswerIsNotValidException"/>
 *     &lt;enumeration value="SenderDoesNotExistException"/>
 *     &lt;enumeration value="SignatureException"/>
 *     &lt;enumeration value="TooManyAttemptsException"/>
 *     &lt;enumeration value="TooManyWalletsException"/>
 *     &lt;enumeration value="TransactionFailureException"/>
 *     &lt;enumeration value="TransactionIsNotAvailableException"/>
 *     &lt;enumeration value="TypeWrongExceptionDB"/>
 *     &lt;enumeration value="UnsupportedTransaction"/>
 *     &lt;enumeration value="UserBlockedException"/>
 *     &lt;enumeration value="UserDoesNotExistException"/>
 *     &lt;enumeration value="UserAlreadyExistException"/>
 *     &lt;enumeration value="VeryBigAmountException"/>
 *     &lt;enumeration value="WalletCurrencyIncorrectException"/>
 *     &lt;enumeration value="WalletDoesNotExist"/>
 *     &lt;enumeration value="WrongParametersException"/>
 *     &lt;enumeration value="TooManyAttemptsPasswordRecoveryException"/>
 *     &lt;enumeration value="SAMException"/>
 *     &lt;enumeration value="WrongEmailException"/>
 *     &lt;enumeration value="MultipleWalletsException"/>
 *     &lt;enumeration value="IpInBlackListException"/>
 *     &lt;enumeration value="CardDoesNotExistException"/>
 *     &lt;enumeration value="CardIsNotActiveException"/>
 *     &lt;enumeration value="CardNumberIsNotValidException"/>
 *     &lt;enumeration value="ProtectimusAuthenticationException"/>
 *     &lt;enumeration value="AccountIsNotVerifiedException"/>
 *     &lt;enumeration value="UserSuspiciousException"/>
 *     &lt;enumeration value="OkPayApiException"/>
 *     &lt;enumeration value="PerfectMoneyApiException"/>
 *     &lt;enumeration value="EgoPayApiException"/>
 *     &lt;enumeration value="PaxumApiException"/>
 *     &lt;enumeration value="CoinbaseApiException"/>
 *     &lt;enumeration value="YandexMoneyApiException"/>
 *     &lt;enumeration value="PayeerApiException"/>
 *     &lt;enumeration value="NotSupportedCountryException"/>
 *     &lt;enumeration value="BitpayApiException"/>
 *     &lt;enumeration value="ExmoApiException"/>
 *     &lt;enumeration value="FetchDataException"/>
 *     &lt;enumeration value="KitPSApiException"/>
 *     &lt;enumeration value="NotEnoughMoneyApiException"/>
 *     &lt;enumeration value="TestPaymentApiException"/>
 *     &lt;enumeration value="NotSupportedBankBinException"/>
 *     &lt;enumeration value="WebMoneyApiException"/>
 *     &lt;enumeration value="QiwiApiException"/>
 *     &lt;enumeration value="DuplicateOrderIdException"/>
 *     &lt;enumeration value="MixPlatApiException"/>
 *     &lt;enumeration value="BtcEApiException"/>
 *     &lt;enumeration value="NetexApiException"/>
 *     &lt;enumeration value="CardNotEnoughMoneyException"/>
 *     &lt;enumeration value="TransactionTemporaryNotAvailableException"/>
 *     &lt;enumeration value="InterkassaApiException"/>
 *     &lt;enumeration value="EcoinApiException"/>
 *     &lt;enumeration value="DuplicateTransactionException"/>
 *     &lt;enumeration value="MonetaruApiException"/>
 *     &lt;enumeration value="CardBlockException"/>
 *     &lt;enumeration value="CapitalistApiException"/>
 *     &lt;enumeration value="CardsApiDisabledException"/>
 *     &lt;enumeration value="InternalException"/>
 *     &lt;enumeration value="LimitsException"/>
 *     &lt;enumeration value="NotAuthException"/>
 *     &lt;enumeration value="WrongParamsException"/>
 *     &lt;enumeration value="AccountIdDoesNotBelongToRequesterException"/>
 *     &lt;enumeration value="UserIsAlreadyVerifiedException"/>
 *     &lt;enumeration value="CardsAPISmsSendRestrictionException"/>
 *     &lt;enumeration value="ChangeCardStatusException"/>
 *     &lt;enumeration value="CardActivationException"/>
 *     &lt;enumeration value="AdditionalDataRequiredException"/>
 *     &lt;enumeration value="SystemAccountDeleteException"/>
 *     &lt;enumeration value="VerificationRequestIsAlreadySubmitted"/>
 *     &lt;enumeration value="BitPayException"/>
 *     &lt;enumeration value="ChangeSystemAccountTypeRequestException"/>
 *     &lt;enumeration value="BitokApiException"/>
 *     &lt;enumeration value="PayzaApiException"/>
 *     &lt;enumeration value="EpeseApiException"/>
 *     &lt;enumeration value="PayboxKzApiException"/>
 *     &lt;enumeration value="PayKeeperApiException"/>
 *     &lt;enumeration value="BitOneApiException"/>
 *     &lt;enumeration value="DuplicateDocumentNumberDigitsException"/>
 *     &lt;enumeration value="GpaysafeApiException"/>
 *     &lt;enumeration value="CreditPilotAPIException"/>
 *     &lt;enumeration value="RsbApiException"/>
 *     &lt;enumeration value="AlliedWalletApiException"/>
 *     &lt;enumeration value="UnauthorizedIpAddressException"/>
 *     &lt;enumeration value="WinPayApiException"/>
 *     &lt;enumeration value="YandexCassaApiException"/>
 *     &lt;enumeration value="EpayApiException"/>
 *     &lt;enumeration value="TicketSystemApiException"/>
 *     &lt;enumeration value="ParseErrorException"/>
 *     &lt;enumeration value="LimitPerCardPerDayException"/>
 *     &lt;enumeration value="LifetimeLimitException"/>
 *     &lt;enumeration value="EcoinVoucherApiException"/>
 *     &lt;enumeration value="ClientProfileDoesNotMatchException"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "exceptionType")
@XmlEnum
public enum ExceptionType {

    @XmlEnumValue("AlreadyExistException")
    ALREADY_EXIST_EXCEPTION("AlreadyExistException"),
    @XmlEnumValue("AttemptsHaveEndedException")
    ATTEMPTS_HAVE_ENDED_EXCEPTION("AttemptsHaveEndedException"),
    @XmlEnumValue("BadParametersException")
    BAD_PARAMETERS_EXCEPTION("BadParametersException"),
    @XmlEnumValue("BadFormatAmountException")
    BAD_FORMAT_AMOUNT_EXCEPTION("BadFormatAmountException"),
    @XmlEnumValue("CodeHasExpired")
    CODE_HAS_EXPIRED("CodeHasExpired"),
    @XmlEnumValue("CodeIsNotValidException")
    CODE_IS_NOT_VALID_EXCEPTION("CodeIsNotValidException"),
    @XmlEnumValue("CryptoException")
    CRYPTO_EXCEPTION("CryptoException"),
    @XmlEnumValue("DatabaseException")
    DATABASE_EXCEPTION("DatabaseException"),
    @XmlEnumValue("EmailAlreadyExistException")
    EMAIL_ALREADY_EXIST_EXCEPTION("EmailAlreadyExistException"),
    @XmlEnumValue("ExchangeCurrencyException")
    EXCHANGE_CURRENCY_EXCEPTION("ExchangeCurrencyException"),
    @XmlEnumValue("FileFormatException")
    FILE_FORMAT_EXCEPTION("FileFormatException"),
    @XmlEnumValue("FileSystemException")
    FILE_SYSTEM_EXCEPTION("FileSystemException"),
    @XmlEnumValue("GroupDoesNotEmptyException")
    GROUP_DOES_NOT_EMPTY_EXCEPTION("GroupDoesNotEmptyException"),
    @XmlEnumValue("IpAddressIsWrongException")
    IP_ADDRESS_IS_WRONG_EXCEPTION("IpAddressIsWrongException"),
    @XmlEnumValue("LimitPerMonthException")
    LIMIT_PER_MONTH_EXCEPTION("LimitPerMonthException"),
    @XmlEnumValue("LimitPerDayException")
    LIMIT_PER_DAY_EXCEPTION("LimitPerDayException"),
    @XmlEnumValue("LimitPerTransactionException")
    LIMIT_PER_TRANSACTION_EXCEPTION("LimitPerTransactionException"),
    @XmlEnumValue("LoginAlreadyExistException")
    LOGIN_ALREADY_EXIST_EXCEPTION("LoginAlreadyExistException"),
    @XmlEnumValue("NotEnoughMoneyException")
    NOT_ENOUGH_MONEY_EXCEPTION("NotEnoughMoneyException"),
    @XmlEnumValue("PasswordExpiredException")
    PASSWORD_EXPIRED_EXCEPTION("PasswordExpiredException"),
    @XmlEnumValue("PasswordIncorrectException")
    PASSWORD_INCORRECT_EXCEPTION("PasswordIncorrectException"),
    @XmlEnumValue("PasswordIsNotNewException")
    PASSWORD_IS_NOT_NEW_EXCEPTION("PasswordIsNotNewException"),
    @XmlEnumValue("PINCodeIncorrectException")
    PIN_CODE_INCORRECT_EXCEPTION("PINCodeIncorrectException"),
    @XmlEnumValue("PasswordRecoveryException")
    PASSWORD_RECOVERY_EXCEPTION("PasswordRecoveryException"),
    @XmlEnumValue("PhoneNumberAlreadyExistException")
    PHONE_NUMBER_ALREADY_EXIST_EXCEPTION("PhoneNumberAlreadyExistException"),
    @XmlEnumValue("PhoneNumberNotValidException")
    PHONE_NUMBER_NOT_VALID_EXCEPTION("PhoneNumberNotValidException"),
    @XmlEnumValue("RecipientDoesNotExistException")
    RECIPIENT_DOES_NOT_EXIST_EXCEPTION("RecipientDoesNotExistException"),
    @XmlEnumValue("RegistrationException")
    REGISTRATION_EXCEPTION("RegistrationException"),
    @XmlEnumValue("SecretAnswerIsNotValidException")
    SECRET_ANSWER_IS_NOT_VALID_EXCEPTION("SecretAnswerIsNotValidException"),
    @XmlEnumValue("SenderDoesNotExistException")
    SENDER_DOES_NOT_EXIST_EXCEPTION("SenderDoesNotExistException"),
    @XmlEnumValue("SignatureException")
    SIGNATURE_EXCEPTION("SignatureException"),
    @XmlEnumValue("TooManyAttemptsException")
    TOO_MANY_ATTEMPTS_EXCEPTION("TooManyAttemptsException"),
    @XmlEnumValue("TooManyWalletsException")
    TOO_MANY_WALLETS_EXCEPTION("TooManyWalletsException"),
    @XmlEnumValue("TransactionFailureException")
    TRANSACTION_FAILURE_EXCEPTION("TransactionFailureException"),
    @XmlEnumValue("TransactionIsNotAvailableException")
    TRANSACTION_IS_NOT_AVAILABLE_EXCEPTION("TransactionIsNotAvailableException"),
    @XmlEnumValue("TypeWrongExceptionDB")
    TYPE_WRONG_EXCEPTION_DB("TypeWrongExceptionDB"),
    @XmlEnumValue("UnsupportedTransaction")
    UNSUPPORTED_TRANSACTION("UnsupportedTransaction"),
    @XmlEnumValue("UserBlockedException")
    USER_BLOCKED_EXCEPTION("UserBlockedException"),
    @XmlEnumValue("UserDoesNotExistException")
    USER_DOES_NOT_EXIST_EXCEPTION("UserDoesNotExistException"),
    @XmlEnumValue("UserAlreadyExistException")
    USER_ALREADY_EXIST_EXCEPTION("UserAlreadyExistException"),
    @XmlEnumValue("VeryBigAmountException")
    VERY_BIG_AMOUNT_EXCEPTION("VeryBigAmountException"),
    @XmlEnumValue("WalletCurrencyIncorrectException")
    WALLET_CURRENCY_INCORRECT_EXCEPTION("WalletCurrencyIncorrectException"),
    @XmlEnumValue("WalletDoesNotExist")
    WALLET_DOES_NOT_EXIST("WalletDoesNotExist"),
    @XmlEnumValue("WrongParametersException")
    WRONG_PARAMETERS_EXCEPTION("WrongParametersException"),
    @XmlEnumValue("TooManyAttemptsPasswordRecoveryException")
    TOO_MANY_ATTEMPTS_PASSWORD_RECOVERY_EXCEPTION("TooManyAttemptsPasswordRecoveryException"),
    @XmlEnumValue("SAMException")
    SAM_EXCEPTION("SAMException"),
    @XmlEnumValue("WrongEmailException")
    WRONG_EMAIL_EXCEPTION("WrongEmailException"),
    @XmlEnumValue("MultipleWalletsException")
    MULTIPLE_WALLETS_EXCEPTION("MultipleWalletsException"),
    @XmlEnumValue("IpInBlackListException")
    IP_IN_BLACK_LIST_EXCEPTION("IpInBlackListException"),
    @XmlEnumValue("CardDoesNotExistException")
    CARD_DOES_NOT_EXIST_EXCEPTION("CardDoesNotExistException"),
    @XmlEnumValue("CardIsNotActiveException")
    CARD_IS_NOT_ACTIVE_EXCEPTION("CardIsNotActiveException"),
    @XmlEnumValue("CardNumberIsNotValidException")
    CARD_NUMBER_IS_NOT_VALID_EXCEPTION("CardNumberIsNotValidException"),
    @XmlEnumValue("ProtectimusAuthenticationException")
    PROTECTIMUS_AUTHENTICATION_EXCEPTION("ProtectimusAuthenticationException"),
    @XmlEnumValue("AccountIsNotVerifiedException")
    ACCOUNT_IS_NOT_VERIFIED_EXCEPTION("AccountIsNotVerifiedException"),
    @XmlEnumValue("UserSuspiciousException")
    USER_SUSPICIOUS_EXCEPTION("UserSuspiciousException"),
    @XmlEnumValue("OkPayApiException")
    OK_PAY_API_EXCEPTION("OkPayApiException"),
    @XmlEnumValue("PerfectMoneyApiException")
    PERFECT_MONEY_API_EXCEPTION("PerfectMoneyApiException"),
    @XmlEnumValue("EgoPayApiException")
    EGO_PAY_API_EXCEPTION("EgoPayApiException"),
    @XmlEnumValue("PaxumApiException")
    PAXUM_API_EXCEPTION("PaxumApiException"),
    @XmlEnumValue("CoinbaseApiException")
    COINBASE_API_EXCEPTION("CoinbaseApiException"),
    @XmlEnumValue("YandexMoneyApiException")
    YANDEX_MONEY_API_EXCEPTION("YandexMoneyApiException"),
    @XmlEnumValue("PayeerApiException")
    PAYEER_API_EXCEPTION("PayeerApiException"),
    @XmlEnumValue("NotSupportedCountryException")
    NOT_SUPPORTED_COUNTRY_EXCEPTION("NotSupportedCountryException"),
    @XmlEnumValue("BitpayApiException")
    BITPAY_API_EXCEPTION("BitpayApiException"),
    @XmlEnumValue("ExmoApiException")
    EXMO_API_EXCEPTION("ExmoApiException"),
    @XmlEnumValue("FetchDataException")
    FETCH_DATA_EXCEPTION("FetchDataException"),
    @XmlEnumValue("KitPSApiException")
    KIT_PS_API_EXCEPTION("KitPSApiException"),
    @XmlEnumValue("NotEnoughMoneyApiException")
    NOT_ENOUGH_MONEY_API_EXCEPTION("NotEnoughMoneyApiException"),
    @XmlEnumValue("TestPaymentApiException")
    TEST_PAYMENT_API_EXCEPTION("TestPaymentApiException"),
    @XmlEnumValue("NotSupportedBankBinException")
    NOT_SUPPORTED_BANK_BIN_EXCEPTION("NotSupportedBankBinException"),
    @XmlEnumValue("WebMoneyApiException")
    WEB_MONEY_API_EXCEPTION("WebMoneyApiException"),
    @XmlEnumValue("QiwiApiException")
    QIWI_API_EXCEPTION("QiwiApiException"),
    @XmlEnumValue("DuplicateOrderIdException")
    DUPLICATE_ORDER_ID_EXCEPTION("DuplicateOrderIdException"),
    @XmlEnumValue("MixPlatApiException")
    MIX_PLAT_API_EXCEPTION("MixPlatApiException"),
    @XmlEnumValue("BtcEApiException")
    BTC_E_API_EXCEPTION("BtcEApiException"),
    @XmlEnumValue("NetexApiException")
    NETEX_API_EXCEPTION("NetexApiException"),
    @XmlEnumValue("CardNotEnoughMoneyException")
    CARD_NOT_ENOUGH_MONEY_EXCEPTION("CardNotEnoughMoneyException"),
    @XmlEnumValue("TransactionTemporaryNotAvailableException")
    TRANSACTION_TEMPORARY_NOT_AVAILABLE_EXCEPTION("TransactionTemporaryNotAvailableException"),
    @XmlEnumValue("InterkassaApiException")
    INTERKASSA_API_EXCEPTION("InterkassaApiException"),
    @XmlEnumValue("EcoinApiException")
    ECOIN_API_EXCEPTION("EcoinApiException"),
    @XmlEnumValue("DuplicateTransactionException")
    DUPLICATE_TRANSACTION_EXCEPTION("DuplicateTransactionException"),
    @XmlEnumValue("MonetaruApiException")
    MONETARU_API_EXCEPTION("MonetaruApiException"),
    @XmlEnumValue("CardBlockException")
    CARD_BLOCK_EXCEPTION("CardBlockException"),
    @XmlEnumValue("CapitalistApiException")
    CAPITALIST_API_EXCEPTION("CapitalistApiException"),
    @XmlEnumValue("CardsApiDisabledException")
    CARDS_API_DISABLED_EXCEPTION("CardsApiDisabledException"),
    @XmlEnumValue("InternalException")
    INTERNAL_EXCEPTION("InternalException"),
    @XmlEnumValue("LimitsException")
    LIMITS_EXCEPTION("LimitsException"),
    @XmlEnumValue("NotAuthException")
    NOT_AUTH_EXCEPTION("NotAuthException"),
    @XmlEnumValue("WrongParamsException")
    WRONG_PARAMS_EXCEPTION("WrongParamsException"),
    @XmlEnumValue("AccountIdDoesNotBelongToRequesterException")
    ACCOUNT_ID_DOES_NOT_BELONG_TO_REQUESTER_EXCEPTION("AccountIdDoesNotBelongToRequesterException"),
    @XmlEnumValue("UserIsAlreadyVerifiedException")
    USER_IS_ALREADY_VERIFIED_EXCEPTION("UserIsAlreadyVerifiedException"),
    @XmlEnumValue("CardsAPISmsSendRestrictionException")
    CARDS_API_SMS_SEND_RESTRICTION_EXCEPTION("CardsAPISmsSendRestrictionException"),
    @XmlEnumValue("ChangeCardStatusException")
    CHANGE_CARD_STATUS_EXCEPTION("ChangeCardStatusException"),
    @XmlEnumValue("CardActivationException")
    CARD_ACTIVATION_EXCEPTION("CardActivationException"),
    @XmlEnumValue("AdditionalDataRequiredException")
    ADDITIONAL_DATA_REQUIRED_EXCEPTION("AdditionalDataRequiredException"),
    @XmlEnumValue("SystemAccountDeleteException")
    SYSTEM_ACCOUNT_DELETE_EXCEPTION("SystemAccountDeleteException"),
    @XmlEnumValue("VerificationRequestIsAlreadySubmitted")
    VERIFICATION_REQUEST_IS_ALREADY_SUBMITTED("VerificationRequestIsAlreadySubmitted"),
    @XmlEnumValue("BitPayException")
    BIT_PAY_EXCEPTION("BitPayException"),
    @XmlEnumValue("ChangeSystemAccountTypeRequestException")
    CHANGE_SYSTEM_ACCOUNT_TYPE_REQUEST_EXCEPTION("ChangeSystemAccountTypeRequestException"),
    @XmlEnumValue("BitokApiException")
    BITOK_API_EXCEPTION("BitokApiException"),
    @XmlEnumValue("PayzaApiException")
    PAYZA_API_EXCEPTION("PayzaApiException"),
    @XmlEnumValue("EpeseApiException")
    EPESE_API_EXCEPTION("EpeseApiException"),
    @XmlEnumValue("PayboxKzApiException")
    PAYBOX_KZ_API_EXCEPTION("PayboxKzApiException"),
    @XmlEnumValue("PayKeeperApiException")
    PAY_KEEPER_API_EXCEPTION("PayKeeperApiException"),
    @XmlEnumValue("BitOneApiException")
    BIT_ONE_API_EXCEPTION("BitOneApiException"),
    @XmlEnumValue("DuplicateDocumentNumberDigitsException")
    DUPLICATE_DOCUMENT_NUMBER_DIGITS_EXCEPTION("DuplicateDocumentNumberDigitsException"),
    @XmlEnumValue("GpaysafeApiException")
    GPAYSAFE_API_EXCEPTION("GpaysafeApiException"),
    @XmlEnumValue("CreditPilotAPIException")
    CREDIT_PILOT_API_EXCEPTION("CreditPilotAPIException"),
    @XmlEnumValue("RsbApiException")
    RSB_API_EXCEPTION("RsbApiException"),
    @XmlEnumValue("AlliedWalletApiException")
    ALLIED_WALLET_API_EXCEPTION("AlliedWalletApiException"),
    @XmlEnumValue("UnauthorizedIpAddressException")
    UNAUTHORIZED_IP_ADDRESS_EXCEPTION("UnauthorizedIpAddressException"),
    @XmlEnumValue("WinPayApiException")
    WIN_PAY_API_EXCEPTION("WinPayApiException"),
    @XmlEnumValue("YandexCassaApiException")
    YANDEX_CASSA_API_EXCEPTION("YandexCassaApiException"),
    @XmlEnumValue("EpayApiException")
    EPAY_API_EXCEPTION("EpayApiException"),
    @XmlEnumValue("TicketSystemApiException")
    TICKET_SYSTEM_API_EXCEPTION("TicketSystemApiException"),
    @XmlEnumValue("ParseErrorException")
    PARSE_ERROR_EXCEPTION("ParseErrorException"),
    @XmlEnumValue("LimitPerCardPerDayException")
    LIMIT_PER_CARD_PER_DAY_EXCEPTION("LimitPerCardPerDayException"),
    @XmlEnumValue("LifetimeLimitException")
    LIFETIME_LIMIT_EXCEPTION("LifetimeLimitException"),
    @XmlEnumValue("EcoinVoucherApiException")
    ECOIN_VOUCHER_API_EXCEPTION("EcoinVoucherApiException"),
    @XmlEnumValue("ClientProfileDoesNotMatchException")
    CLIENT_PROFILE_DOES_NOT_MATCH_EXCEPTION("ClientProfileDoesNotMatchException");
    private final String value;

    ExceptionType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ExceptionType fromValue(String v) {
        for (ExceptionType c: ExceptionType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
