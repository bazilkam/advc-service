//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2017.12.28 at 02:05:26 AM EET 
//


package ua.nano.soap.wsm;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for advcashCardTransferRequestDTO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="advcashCardTransferRequestDTO"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="amount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *         &lt;element name="cardType" type="{http://wsm.advcash/}cardType" minOccurs="0"/&gt;
 *         &lt;element name="currency" type="{http://wsm.advcash/}currency" minOccurs="0"/&gt;
 *         &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="savePaymentTemplate" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="srcWalletId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "advcashCardTransferRequestDTO", propOrder = {
    "amount",
    "cardType",
    "currency",
    "email",
    "savePaymentTemplate",
    "srcWalletId"
})
public class AdvcashCardTransferRequestDTO {

    protected BigDecimal amount;
    @XmlSchemaType(name = "string")
    protected CardType cardType;
    @XmlSchemaType(name = "string")
    protected Currency currency;
    protected String email;
    protected boolean savePaymentTemplate;
    protected String srcWalletId;

    /**
     * Gets the value of the amount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * Sets the value of the amount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAmount(BigDecimal value) {
        this.amount = value;
    }

    /**
     * Gets the value of the cardType property.
     * 
     * @return
     *     possible object is
     *     {@link CardType }
     *     
     */
    public CardType getCardType() {
        return cardType;
    }

    /**
     * Sets the value of the cardType property.
     * 
     * @param value
     *     allowed object is
     *     {@link CardType }
     *     
     */
    public void setCardType(CardType value) {
        this.cardType = value;
    }

    /**
     * Gets the value of the currency property.
     * 
     * @return
     *     possible object is
     *     {@link Currency }
     *     
     */
    public Currency getCurrency() {
        return currency;
    }

    /**
     * Sets the value of the currency property.
     * 
     * @param value
     *     allowed object is
     *     {@link Currency }
     *     
     */
    public void setCurrency(Currency value) {
        this.currency = value;
    }

    /**
     * Gets the value of the email property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmail() {
        return email;
    }

    /**
     * Sets the value of the email property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmail(String value) {
        this.email = value;
    }

    /**
     * Gets the value of the savePaymentTemplate property.
     * 
     */
    public boolean isSavePaymentTemplate() {
        return savePaymentTemplate;
    }

    /**
     * Sets the value of the savePaymentTemplate property.
     * 
     */
    public void setSavePaymentTemplate(boolean value) {
        this.savePaymentTemplate = value;
    }

    /**
     * Gets the value of the srcWalletId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSrcWalletId() {
        return srcWalletId;
    }

    /**
     * Sets the value of the srcWalletId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSrcWalletId(String value) {
        this.srcWalletId = value;
    }

}
