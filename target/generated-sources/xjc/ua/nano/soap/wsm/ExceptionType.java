//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2017.12.28 at 02:05:26 AM EET 
//


package ua.nano.soap.wsm;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for exceptionType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="exceptionType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="AlreadyExistException"/&gt;
 *     &lt;enumeration value="AttemptsHaveEndedException"/&gt;
 *     &lt;enumeration value="BadParametersException"/&gt;
 *     &lt;enumeration value="BadFormatAmountException"/&gt;
 *     &lt;enumeration value="CodeHasExpired"/&gt;
 *     &lt;enumeration value="CodeIsNotValidException"/&gt;
 *     &lt;enumeration value="CryptoException"/&gt;
 *     &lt;enumeration value="DatabaseException"/&gt;
 *     &lt;enumeration value="EmailAlreadyExistException"/&gt;
 *     &lt;enumeration value="ExchangeCurrencyException"/&gt;
 *     &lt;enumeration value="FileFormatException"/&gt;
 *     &lt;enumeration value="FileSystemException"/&gt;
 *     &lt;enumeration value="GroupDoesNotEmptyException"/&gt;
 *     &lt;enumeration value="IpAddressIsWrongException"/&gt;
 *     &lt;enumeration value="LimitPerMonthException"/&gt;
 *     &lt;enumeration value="LimitPerDayException"/&gt;
 *     &lt;enumeration value="LimitPerTransactionException"/&gt;
 *     &lt;enumeration value="LoginAlreadyExistException"/&gt;
 *     &lt;enumeration value="NotEnoughMoneyException"/&gt;
 *     &lt;enumeration value="PasswordExpiredException"/&gt;
 *     &lt;enumeration value="PasswordIncorrectException"/&gt;
 *     &lt;enumeration value="PasswordIsNotNewException"/&gt;
 *     &lt;enumeration value="PINCodeIncorrectException"/&gt;
 *     &lt;enumeration value="PasswordRecoveryException"/&gt;
 *     &lt;enumeration value="PhoneNumberAlreadyExistException"/&gt;
 *     &lt;enumeration value="PhoneNumberNotValidException"/&gt;
 *     &lt;enumeration value="RecipientDoesNotExistException"/&gt;
 *     &lt;enumeration value="RegistrationException"/&gt;
 *     &lt;enumeration value="SecretAnswerIsNotValidException"/&gt;
 *     &lt;enumeration value="SenderDoesNotExistException"/&gt;
 *     &lt;enumeration value="SignatureException"/&gt;
 *     &lt;enumeration value="TooManyAttemptsException"/&gt;
 *     &lt;enumeration value="TooManyWalletsException"/&gt;
 *     &lt;enumeration value="TransactionFailureException"/&gt;
 *     &lt;enumeration value="TransactionIsNotAvailableException"/&gt;
 *     &lt;enumeration value="TypeWrongExceptionDB"/&gt;
 *     &lt;enumeration value="UnsupportedTransaction"/&gt;
 *     &lt;enumeration value="UserBlockedException"/&gt;
 *     &lt;enumeration value="UserDoesNotExistException"/&gt;
 *     &lt;enumeration value="UserAlreadyExistException"/&gt;
 *     &lt;enumeration value="VeryBigAmountException"/&gt;
 *     &lt;enumeration value="WalletCurrencyIncorrectException"/&gt;
 *     &lt;enumeration value="WalletDoesNotExist"/&gt;
 *     &lt;enumeration value="WrongParametersException"/&gt;
 *     &lt;enumeration value="TooManyAttemptsPasswordRecoveryException"/&gt;
 *     &lt;enumeration value="SAMException"/&gt;
 *     &lt;enumeration value="WrongEmailException"/&gt;
 *     &lt;enumeration value="MultipleWalletsException"/&gt;
 *     &lt;enumeration value="IpInBlackListException"/&gt;
 *     &lt;enumeration value="CardDoesNotExistException"/&gt;
 *     &lt;enumeration value="CardIsNotActiveException"/&gt;
 *     &lt;enumeration value="CardNumberIsNotValidException"/&gt;
 *     &lt;enumeration value="ProtectimusAuthenticationException"/&gt;
 *     &lt;enumeration value="AccountIsNotVerifiedException"/&gt;
 *     &lt;enumeration value="UserSuspiciousException"/&gt;
 *     &lt;enumeration value="OkPayApiException"/&gt;
 *     &lt;enumeration value="PerfectMoneyApiException"/&gt;
 *     &lt;enumeration value="EgoPayApiException"/&gt;
 *     &lt;enumeration value="PaxumApiException"/&gt;
 *     &lt;enumeration value="CoinbaseApiException"/&gt;
 *     &lt;enumeration value="YandexMoneyApiException"/&gt;
 *     &lt;enumeration value="PayeerApiException"/&gt;
 *     &lt;enumeration value="NotSupportedCountryException"/&gt;
 *     &lt;enumeration value="BitpayApiException"/&gt;
 *     &lt;enumeration value="ExmoApiException"/&gt;
 *     &lt;enumeration value="FetchDataException"/&gt;
 *     &lt;enumeration value="KitPSApiException"/&gt;
 *     &lt;enumeration value="NotEnoughMoneyApiException"/&gt;
 *     &lt;enumeration value="TestPaymentApiException"/&gt;
 *     &lt;enumeration value="NotSupportedBankBinException"/&gt;
 *     &lt;enumeration value="WebMoneyApiException"/&gt;
 *     &lt;enumeration value="QiwiApiException"/&gt;
 *     &lt;enumeration value="DuplicateOrderIdException"/&gt;
 *     &lt;enumeration value="MixPlatApiException"/&gt;
 *     &lt;enumeration value="BtcEApiException"/&gt;
 *     &lt;enumeration value="NetexApiException"/&gt;
 *     &lt;enumeration value="CardNotEnoughMoneyException"/&gt;
 *     &lt;enumeration value="TransactionTemporaryNotAvailableException"/&gt;
 *     &lt;enumeration value="InterkassaApiException"/&gt;
 *     &lt;enumeration value="EcoinApiException"/&gt;
 *     &lt;enumeration value="DuplicateTransactionException"/&gt;
 *     &lt;enumeration value="MonetaruApiException"/&gt;
 *     &lt;enumeration value="CardBlockException"/&gt;
 *     &lt;enumeration value="CapitalistApiException"/&gt;
 *     &lt;enumeration value="CardsApiDisabledException"/&gt;
 *     &lt;enumeration value="InternalException"/&gt;
 *     &lt;enumeration value="LimitsException"/&gt;
 *     &lt;enumeration value="NotAuthException"/&gt;
 *     &lt;enumeration value="WrongParamsException"/&gt;
 *     &lt;enumeration value="AccountIdDoesNotBelongToRequesterException"/&gt;
 *     &lt;enumeration value="UserIsAlreadyVerifiedException"/&gt;
 *     &lt;enumeration value="CardsAPISmsSendRestrictionException"/&gt;
 *     &lt;enumeration value="ChangeCardStatusException"/&gt;
 *     &lt;enumeration value="CardActivationException"/&gt;
 *     &lt;enumeration value="AdditionalDataRequiredException"/&gt;
 *     &lt;enumeration value="SystemAccountDeleteException"/&gt;
 *     &lt;enumeration value="VerificationRequestIsAlreadySubmitted"/&gt;
 *     &lt;enumeration value="BitPayException"/&gt;
 *     &lt;enumeration value="ChangeSystemAccountTypeRequestException"/&gt;
 *     &lt;enumeration value="BitokApiException"/&gt;
 *     &lt;enumeration value="PayzaApiException"/&gt;
 *     &lt;enumeration value="EpeseApiException"/&gt;
 *     &lt;enumeration value="PayboxKzApiException"/&gt;
 *     &lt;enumeration value="PayKeeperApiException"/&gt;
 *     &lt;enumeration value="BitOneApiException"/&gt;
 *     &lt;enumeration value="DuplicateDocumentNumberDigitsException"/&gt;
 *     &lt;enumeration value="GpaysafeApiException"/&gt;
 *     &lt;enumeration value="CreditPilotAPIException"/&gt;
 *     &lt;enumeration value="RsbApiException"/&gt;
 *     &lt;enumeration value="AlliedWalletApiException"/&gt;
 *     &lt;enumeration value="UnauthorizedIpAddressException"/&gt;
 *     &lt;enumeration value="WinPayApiException"/&gt;
 *     &lt;enumeration value="YandexCassaApiException"/&gt;
 *     &lt;enumeration value="EpayApiException"/&gt;
 *     &lt;enumeration value="TicketSystemApiException"/&gt;
 *     &lt;enumeration value="ParseErrorException"/&gt;
 *     &lt;enumeration value="LimitPerCardPerDayException"/&gt;
 *     &lt;enumeration value="LifetimeLimitException"/&gt;
 *     &lt;enumeration value="EcoinVoucherApiException"/&gt;
 *     &lt;enumeration value="ClientProfileDoesNotMatchException"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "exceptionType")
@XmlEnum
public enum ExceptionType {

    @XmlEnumValue("AlreadyExistException")
    ALREADY_EXIST_EXCEPTION("AlreadyExistException"),
    @XmlEnumValue("AttemptsHaveEndedException")
    ATTEMPTS_HAVE_ENDED_EXCEPTION("AttemptsHaveEndedException"),
    @XmlEnumValue("BadParametersException")
    BAD_PARAMETERS_EXCEPTION("BadParametersException"),
    @XmlEnumValue("BadFormatAmountException")
    BAD_FORMAT_AMOUNT_EXCEPTION("BadFormatAmountException"),
    @XmlEnumValue("CodeHasExpired")
    CODE_HAS_EXPIRED("CodeHasExpired"),
    @XmlEnumValue("CodeIsNotValidException")
    CODE_IS_NOT_VALID_EXCEPTION("CodeIsNotValidException"),
    @XmlEnumValue("CryptoException")
    CRYPTO_EXCEPTION("CryptoException"),
    @XmlEnumValue("DatabaseException")
    DATABASE_EXCEPTION("DatabaseException"),
    @XmlEnumValue("EmailAlreadyExistException")
    EMAIL_ALREADY_EXIST_EXCEPTION("EmailAlreadyExistException"),
    @XmlEnumValue("ExchangeCurrencyException")
    EXCHANGE_CURRENCY_EXCEPTION("ExchangeCurrencyException"),
    @XmlEnumValue("FileFormatException")
    FILE_FORMAT_EXCEPTION("FileFormatException"),
    @XmlEnumValue("FileSystemException")
    FILE_SYSTEM_EXCEPTION("FileSystemException"),
    @XmlEnumValue("GroupDoesNotEmptyException")
    GROUP_DOES_NOT_EMPTY_EXCEPTION("GroupDoesNotEmptyException"),
    @XmlEnumValue("IpAddressIsWrongException")
    IP_ADDRESS_IS_WRONG_EXCEPTION("IpAddressIsWrongException"),
    @XmlEnumValue("LimitPerMonthException")
    LIMIT_PER_MONTH_EXCEPTION("LimitPerMonthException"),
    @XmlEnumValue("LimitPerDayException")
    LIMIT_PER_DAY_EXCEPTION("LimitPerDayException"),
    @XmlEnumValue("LimitPerTransactionException")
    LIMIT_PER_TRANSACTION_EXCEPTION("LimitPerTransactionException"),
    @XmlEnumValue("LoginAlreadyExistException")
    LOGIN_ALREADY_EXIST_EXCEPTION("LoginAlreadyExistException"),
    @XmlEnumValue("NotEnoughMoneyException")
    NOT_ENOUGH_MONEY_EXCEPTION("NotEnoughMoneyException"),
    @XmlEnumValue("PasswordExpiredException")
    PASSWORD_EXPIRED_EXCEPTION("PasswordExpiredException"),
    @XmlEnumValue("PasswordIncorrectException")
    PASSWORD_INCORRECT_EXCEPTION("PasswordIncorrectException"),
    @XmlEnumValue("PasswordIsNotNewException")
    PASSWORD_IS_NOT_NEW_EXCEPTION("PasswordIsNotNewException"),
    @XmlEnumValue("PINCodeIncorrectException")
    PIN_CODE_INCORRECT_EXCEPTION("PINCodeIncorrectException"),
    @XmlEnumValue("PasswordRecoveryException")
    PASSWORD_RECOVERY_EXCEPTION("PasswordRecoveryException"),
    @XmlEnumValue("PhoneNumberAlreadyExistException")
    PHONE_NUMBER_ALREADY_EXIST_EXCEPTION("PhoneNumberAlreadyExistException"),
    @XmlEnumValue("PhoneNumberNotValidException")
    PHONE_NUMBER_NOT_VALID_EXCEPTION("PhoneNumberNotValidException"),
    @XmlEnumValue("RecipientDoesNotExistException")
    RECIPIENT_DOES_NOT_EXIST_EXCEPTION("RecipientDoesNotExistException"),
    @XmlEnumValue("RegistrationException")
    REGISTRATION_EXCEPTION("RegistrationException"),
    @XmlEnumValue("SecretAnswerIsNotValidException")
    SECRET_ANSWER_IS_NOT_VALID_EXCEPTION("SecretAnswerIsNotValidException"),
    @XmlEnumValue("SenderDoesNotExistException")
    SENDER_DOES_NOT_EXIST_EXCEPTION("SenderDoesNotExistException"),
    @XmlEnumValue("SignatureException")
    SIGNATURE_EXCEPTION("SignatureException"),
    @XmlEnumValue("TooManyAttemptsException")
    TOO_MANY_ATTEMPTS_EXCEPTION("TooManyAttemptsException"),
    @XmlEnumValue("TooManyWalletsException")
    TOO_MANY_WALLETS_EXCEPTION("TooManyWalletsException"),
    @XmlEnumValue("TransactionFailureException")
    TRANSACTION_FAILURE_EXCEPTION("TransactionFailureException"),
    @XmlEnumValue("TransactionIsNotAvailableException")
    TRANSACTION_IS_NOT_AVAILABLE_EXCEPTION("TransactionIsNotAvailableException"),
    @XmlEnumValue("TypeWrongExceptionDB")
    TYPE_WRONG_EXCEPTION_DB("TypeWrongExceptionDB"),
    @XmlEnumValue("UnsupportedTransaction")
    UNSUPPORTED_TRANSACTION("UnsupportedTransaction"),
    @XmlEnumValue("UserBlockedException")
    USER_BLOCKED_EXCEPTION("UserBlockedException"),
    @XmlEnumValue("UserDoesNotExistException")
    USER_DOES_NOT_EXIST_EXCEPTION("UserDoesNotExistException"),
    @XmlEnumValue("UserAlreadyExistException")
    USER_ALREADY_EXIST_EXCEPTION("UserAlreadyExistException"),
    @XmlEnumValue("VeryBigAmountException")
    VERY_BIG_AMOUNT_EXCEPTION("VeryBigAmountException"),
    @XmlEnumValue("WalletCurrencyIncorrectException")
    WALLET_CURRENCY_INCORRECT_EXCEPTION("WalletCurrencyIncorrectException"),
    @XmlEnumValue("WalletDoesNotExist")
    WALLET_DOES_NOT_EXIST("WalletDoesNotExist"),
    @XmlEnumValue("WrongParametersException")
    WRONG_PARAMETERS_EXCEPTION("WrongParametersException"),
    @XmlEnumValue("TooManyAttemptsPasswordRecoveryException")
    TOO_MANY_ATTEMPTS_PASSWORD_RECOVERY_EXCEPTION("TooManyAttemptsPasswordRecoveryException"),
    @XmlEnumValue("SAMException")
    SAM_EXCEPTION("SAMException"),
    @XmlEnumValue("WrongEmailException")
    WRONG_EMAIL_EXCEPTION("WrongEmailException"),
    @XmlEnumValue("MultipleWalletsException")
    MULTIPLE_WALLETS_EXCEPTION("MultipleWalletsException"),
    @XmlEnumValue("IpInBlackListException")
    IP_IN_BLACK_LIST_EXCEPTION("IpInBlackListException"),
    @XmlEnumValue("CardDoesNotExistException")
    CARD_DOES_NOT_EXIST_EXCEPTION("CardDoesNotExistException"),
    @XmlEnumValue("CardIsNotActiveException")
    CARD_IS_NOT_ACTIVE_EXCEPTION("CardIsNotActiveException"),
    @XmlEnumValue("CardNumberIsNotValidException")
    CARD_NUMBER_IS_NOT_VALID_EXCEPTION("CardNumberIsNotValidException"),
    @XmlEnumValue("ProtectimusAuthenticationException")
    PROTECTIMUS_AUTHENTICATION_EXCEPTION("ProtectimusAuthenticationException"),
    @XmlEnumValue("AccountIsNotVerifiedException")
    ACCOUNT_IS_NOT_VERIFIED_EXCEPTION("AccountIsNotVerifiedException"),
    @XmlEnumValue("UserSuspiciousException")
    USER_SUSPICIOUS_EXCEPTION("UserSuspiciousException"),
    @XmlEnumValue("OkPayApiException")
    OK_PAY_API_EXCEPTION("OkPayApiException"),
    @XmlEnumValue("PerfectMoneyApiException")
    PERFECT_MONEY_API_EXCEPTION("PerfectMoneyApiException"),
    @XmlEnumValue("EgoPayApiException")
    EGO_PAY_API_EXCEPTION("EgoPayApiException"),
    @XmlEnumValue("PaxumApiException")
    PAXUM_API_EXCEPTION("PaxumApiException"),
    @XmlEnumValue("CoinbaseApiException")
    COINBASE_API_EXCEPTION("CoinbaseApiException"),
    @XmlEnumValue("YandexMoneyApiException")
    YANDEX_MONEY_API_EXCEPTION("YandexMoneyApiException"),
    @XmlEnumValue("PayeerApiException")
    PAYEER_API_EXCEPTION("PayeerApiException"),
    @XmlEnumValue("NotSupportedCountryException")
    NOT_SUPPORTED_COUNTRY_EXCEPTION("NotSupportedCountryException"),
    @XmlEnumValue("BitpayApiException")
    BITPAY_API_EXCEPTION("BitpayApiException"),
    @XmlEnumValue("ExmoApiException")
    EXMO_API_EXCEPTION("ExmoApiException"),
    @XmlEnumValue("FetchDataException")
    FETCH_DATA_EXCEPTION("FetchDataException"),
    @XmlEnumValue("KitPSApiException")
    KIT_PS_API_EXCEPTION("KitPSApiException"),
    @XmlEnumValue("NotEnoughMoneyApiException")
    NOT_ENOUGH_MONEY_API_EXCEPTION("NotEnoughMoneyApiException"),
    @XmlEnumValue("TestPaymentApiException")
    TEST_PAYMENT_API_EXCEPTION("TestPaymentApiException"),
    @XmlEnumValue("NotSupportedBankBinException")
    NOT_SUPPORTED_BANK_BIN_EXCEPTION("NotSupportedBankBinException"),
    @XmlEnumValue("WebMoneyApiException")
    WEB_MONEY_API_EXCEPTION("WebMoneyApiException"),
    @XmlEnumValue("QiwiApiException")
    QIWI_API_EXCEPTION("QiwiApiException"),
    @XmlEnumValue("DuplicateOrderIdException")
    DUPLICATE_ORDER_ID_EXCEPTION("DuplicateOrderIdException"),
    @XmlEnumValue("MixPlatApiException")
    MIX_PLAT_API_EXCEPTION("MixPlatApiException"),
    @XmlEnumValue("BtcEApiException")
    BTC_E_API_EXCEPTION("BtcEApiException"),
    @XmlEnumValue("NetexApiException")
    NETEX_API_EXCEPTION("NetexApiException"),
    @XmlEnumValue("CardNotEnoughMoneyException")
    CARD_NOT_ENOUGH_MONEY_EXCEPTION("CardNotEnoughMoneyException"),
    @XmlEnumValue("TransactionTemporaryNotAvailableException")
    TRANSACTION_TEMPORARY_NOT_AVAILABLE_EXCEPTION("TransactionTemporaryNotAvailableException"),
    @XmlEnumValue("InterkassaApiException")
    INTERKASSA_API_EXCEPTION("InterkassaApiException"),
    @XmlEnumValue("EcoinApiException")
    ECOIN_API_EXCEPTION("EcoinApiException"),
    @XmlEnumValue("DuplicateTransactionException")
    DUPLICATE_TRANSACTION_EXCEPTION("DuplicateTransactionException"),
    @XmlEnumValue("MonetaruApiException")
    MONETARU_API_EXCEPTION("MonetaruApiException"),
    @XmlEnumValue("CardBlockException")
    CARD_BLOCK_EXCEPTION("CardBlockException"),
    @XmlEnumValue("CapitalistApiException")
    CAPITALIST_API_EXCEPTION("CapitalistApiException"),
    @XmlEnumValue("CardsApiDisabledException")
    CARDS_API_DISABLED_EXCEPTION("CardsApiDisabledException"),
    @XmlEnumValue("InternalException")
    INTERNAL_EXCEPTION("InternalException"),
    @XmlEnumValue("LimitsException")
    LIMITS_EXCEPTION("LimitsException"),
    @XmlEnumValue("NotAuthException")
    NOT_AUTH_EXCEPTION("NotAuthException"),
    @XmlEnumValue("WrongParamsException")
    WRONG_PARAMS_EXCEPTION("WrongParamsException"),
    @XmlEnumValue("AccountIdDoesNotBelongToRequesterException")
    ACCOUNT_ID_DOES_NOT_BELONG_TO_REQUESTER_EXCEPTION("AccountIdDoesNotBelongToRequesterException"),
    @XmlEnumValue("UserIsAlreadyVerifiedException")
    USER_IS_ALREADY_VERIFIED_EXCEPTION("UserIsAlreadyVerifiedException"),
    @XmlEnumValue("CardsAPISmsSendRestrictionException")
    CARDS_API_SMS_SEND_RESTRICTION_EXCEPTION("CardsAPISmsSendRestrictionException"),
    @XmlEnumValue("ChangeCardStatusException")
    CHANGE_CARD_STATUS_EXCEPTION("ChangeCardStatusException"),
    @XmlEnumValue("CardActivationException")
    CARD_ACTIVATION_EXCEPTION("CardActivationException"),
    @XmlEnumValue("AdditionalDataRequiredException")
    ADDITIONAL_DATA_REQUIRED_EXCEPTION("AdditionalDataRequiredException"),
    @XmlEnumValue("SystemAccountDeleteException")
    SYSTEM_ACCOUNT_DELETE_EXCEPTION("SystemAccountDeleteException"),
    @XmlEnumValue("VerificationRequestIsAlreadySubmitted")
    VERIFICATION_REQUEST_IS_ALREADY_SUBMITTED("VerificationRequestIsAlreadySubmitted"),
    @XmlEnumValue("BitPayException")
    BIT_PAY_EXCEPTION("BitPayException"),
    @XmlEnumValue("ChangeSystemAccountTypeRequestException")
    CHANGE_SYSTEM_ACCOUNT_TYPE_REQUEST_EXCEPTION("ChangeSystemAccountTypeRequestException"),
    @XmlEnumValue("BitokApiException")
    BITOK_API_EXCEPTION("BitokApiException"),
    @XmlEnumValue("PayzaApiException")
    PAYZA_API_EXCEPTION("PayzaApiException"),
    @XmlEnumValue("EpeseApiException")
    EPESE_API_EXCEPTION("EpeseApiException"),
    @XmlEnumValue("PayboxKzApiException")
    PAYBOX_KZ_API_EXCEPTION("PayboxKzApiException"),
    @XmlEnumValue("PayKeeperApiException")
    PAY_KEEPER_API_EXCEPTION("PayKeeperApiException"),
    @XmlEnumValue("BitOneApiException")
    BIT_ONE_API_EXCEPTION("BitOneApiException"),
    @XmlEnumValue("DuplicateDocumentNumberDigitsException")
    DUPLICATE_DOCUMENT_NUMBER_DIGITS_EXCEPTION("DuplicateDocumentNumberDigitsException"),
    @XmlEnumValue("GpaysafeApiException")
    GPAYSAFE_API_EXCEPTION("GpaysafeApiException"),
    @XmlEnumValue("CreditPilotAPIException")
    CREDIT_PILOT_API_EXCEPTION("CreditPilotAPIException"),
    @XmlEnumValue("RsbApiException")
    RSB_API_EXCEPTION("RsbApiException"),
    @XmlEnumValue("AlliedWalletApiException")
    ALLIED_WALLET_API_EXCEPTION("AlliedWalletApiException"),
    @XmlEnumValue("UnauthorizedIpAddressException")
    UNAUTHORIZED_IP_ADDRESS_EXCEPTION("UnauthorizedIpAddressException"),
    @XmlEnumValue("WinPayApiException")
    WIN_PAY_API_EXCEPTION("WinPayApiException"),
    @XmlEnumValue("YandexCassaApiException")
    YANDEX_CASSA_API_EXCEPTION("YandexCassaApiException"),
    @XmlEnumValue("EpayApiException")
    EPAY_API_EXCEPTION("EpayApiException"),
    @XmlEnumValue("TicketSystemApiException")
    TICKET_SYSTEM_API_EXCEPTION("TicketSystemApiException"),
    @XmlEnumValue("ParseErrorException")
    PARSE_ERROR_EXCEPTION("ParseErrorException"),
    @XmlEnumValue("LimitPerCardPerDayException")
    LIMIT_PER_CARD_PER_DAY_EXCEPTION("LimitPerCardPerDayException"),
    @XmlEnumValue("LifetimeLimitException")
    LIFETIME_LIMIT_EXCEPTION("LifetimeLimitException"),
    @XmlEnumValue("EcoinVoucherApiException")
    ECOIN_VOUCHER_API_EXCEPTION("EcoinVoucherApiException"),
    @XmlEnumValue("ClientProfileDoesNotMatchException")
    CLIENT_PROFILE_DOES_NOT_MATCH_EXCEPTION("ClientProfileDoesNotMatchException");
    private final String value;

    ExceptionType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ExceptionType fromValue(String v) {
        for (ExceptionType c: ExceptionType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
